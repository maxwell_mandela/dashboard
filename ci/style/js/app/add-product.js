$(document).ready(function(){  
    
   /*file input style*/
   $('input[type=file]').bootstrapFileInput();
   $('.file-inputs').bootstrapFileInput();
    
    
    /*DOM MANIP*/
    $(function () {
        $("#product_image").change(function () {
            if($("#product_image")[0].files.length > 4){
                alert('too may files, try maximum of 4!');
            }else{
                $("#dvPreview").empty();
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                //img.attr("style", "height:100px;width: 100px");
                                img.addClass('img-responsive');
                                img.addClass('img-rounded');
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            $("#loading_preview").html(" ");
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        });
    });
    
    function preview(image_dom, image_prv) {
        $(image_dom).change(function () {
            if($(image_dom)[0].files.length > 4){
                alert('too may files, try maximum of 4!');
            }else{
                $(image_prv).empty();
                if (typeof (FileReader) != "undefined") {
                    var dvPreviews = $(image_prv);
                    dvPreviews.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                //img.attr("style", "height:100px;width: 100px");
                                img.addClass('img-responsive');
                                img.addClass('img-rounded');
                                img.attr("src", e.target.result);
                                dvPreviews.append(img);
                            }
                            $("#loading_preview").html(" ");
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreviews.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        });
    }
    
    preview("#front_image", "#front_image_preview");
    preview("#rear_image", "#rear_image_preview");
    preview("#interior_image", "#interior_image_preview");
    preview("#washroom_image", "#washroom_image_preview");
    
    
    $(function () {
        $(".house_images").hide();
        $("#product_category").change(function () {
            if($("#product_category").val() == "Accommodation"){
                $(".one_image").hide();
                $(".house_images").show();
            }else{
                $(".one_image").show();
                $(".house_images").hide();
            }
        });
    });
    
    function checkImageProperties(the_file){
        var img_status;
        var fileUploadControl = $(the_file)[0];
        var file = fileUploadControl.files[0];
        var name = $(the_file).val().replace(/.*(\/|\\)/, '');
        var file_ext = name.substr(name.lastIndexOf('.')+1);
        var alowed_ext = ['jpg','gif','png'];
        var fileSize = file.size;
        if(fileSize < 200000 && $.inArray(file_ext, alowed_ext) != -1 && fileUploadControl.files.length > 0){
            console.log(name+ " type and size accepted");
            img_status = 1;
            return img_status;
        }else{
            toastr.warning("Error: " +name+ " image you've selected is either not an image file type of size greater 200kb!");
            img_status = 0;
            return img_status;
        }
    }
    
    
    $("#sell").click(function(){
       var currentUser = Parse.User.current();
       if (currentUser){
           
           if (currentUser.get("user_phone_number")){
            
            /*if accommodation*/
            if($("#product_category").val() == "Accommodation"){
                var item_images = [];
                var the_url;
                var front_image;
                var house_features = [];
                
                var fileUploadControl = $("#front_image")[0];
                if (checkImageProperties("#front_image") == 1/* && checkImageProperties("#rear_image") == 1 && checkImageProperties("#interior_image") == 1 && checkImageProperties("#washroom_image") == 1*/) {
                  var file = fileUploadControl.files[0];
                  var name = file.name;
                  var front_image = new Parse.File(name, file);

                  waitingDialog.show('Posting item');
                  //toastr.info("Uploading files...");
                  
                  front_image.save().then(function() {
                        item_images.push(front_image._url);
                  /*}).then(function() {
                      var fileUploadControl = $("#rear_image")[0];
                      var rear_image_file = fileUploadControl.files[0];
                      var rear_image_name = rear_image_file.name;
                      checkImageProperties("#rear_image");
                      var rear_image = new Parse.File(rear_image_name, rear_image_file);
                      rear_image.save().then(function() {
                          item_images.push(rear_image._url);
                      });
                              
                  }).then(function() {
                      var fileUploadControl = $("#interior_image")[0];
                      var file = fileUploadControl.files[0];
                      var name = file.name;
                      checkImageProperties("#interior_image");
                      var parseFile = new Parse.File(name, file);
                      parseFile.save().then(function() {
                          item_images.push(parseFile._url);
                      })
                              
                  }).then(function() {
                      var fileUploadControl = $("#washroom_image")[0];
                      var file = fileUploadControl.files[0];
                      var name = file.name;
                      checkImageProperties("#washroom_image");
                      var parseFile = new Parse.File(name, file);
                      parseFile.save().then(function() {
                          item_images.push(parseFile._url);
                          
                      })*/
                      toastr.info("Uploading file finished");
                 }).then(function() {
                      toastr.info("Saving item..."); 
                      var product_title = $("#house_title").val();
                      var product_description = $("#house_description").val();
                      var product_category = $("#product_category").val();
                      var house_size = $("#house_size").val();
                      var item_price = $("#house_price").val();
                      var house_design = $("#house_design").val();
                      var number_of_rooms_available = $("#number_of_rooms_available").val();
                      var owner_name = $("#owner_name").val();
                      var house_type = $("#house_type").val();
                      var payment_means = $("#payment_means").val();
                      
                      //house features
                      var the_features = $("#house_features").val().split('\n');
                      for (var i in the_features) {
                        the_features[i] = the_features[i].trim();
                        house_features.push(the_features[i]);
                      }
                      
                      var product = new Parse.Object("Product");
                      product.set("product_title", product_title);
                      product.set("product_description", product_description);
                      product.set("product_price", item_price);
                      product.set("product_category", product_category);
                      product.set("house_size", house_size);
                      product.set("house_design", house_design);
                      product.set("number_of_rooms_available", number_of_rooms_available);
                      product.set("owner_name", owner_name);
                      product.set("house_type", house_type);
                      product.set("payment_means", payment_means);
                      product.set("house_features", house_features);
                      product.set("sold", false);

                       var currentUser = Parse.User.current();
                       product.set("product_seller_location", currentUser.get('residence'));
                       product.set("product_image", front_image);
                       product.set("item_images", item_images);
                       product.set("user", currentUser);

                       var postACL = new Parse.ACL(currentUser);
                       postACL.setPublicReadAccess(true);
                       postACL.setPublicWriteAccess(false);
                       product.setACL(postACL);

                       if(product_title.length > 0 && product_description.length > 0 && item_price.length > 0 && product_category.length > 0 && house_size.length > 0 && house_design.length > 0 && number_of_rooms_available.length > 0 && owner_name.length > 0 && house_type.length > 0 && house_features.length > 0){
                           product.save(null, {
                            success: function(Products) {
                                waitingDialog.hide();
                                $("#sell").html('<span class="mdi-action-done-all"></span>');
                                toastr.success(product_title+ ' posted successfully!!!');
                                $("#add-new").slideToggle();
                            },
                            error: function(Products, error) {
                                toastr.error('Error Posting item!!');
                                waitingDialog.hide();
                            }
                         });
                       }else{
                           toastr.warning("Please fill all house details before posting!!");
                            waitingDialog.hide();
                    }
                  });
                }else{
                    toastr.error("Please set all images of front, rear, interior and washroom, make sure they are valid image types and not more than 200kb each!");
                    waitingDialog.hide();
                }
            }else{
               /*post var(s)*/
               
               var fileUploadControl = $("#product_image")[0];
               var product_title = $("#product_title").val();
               var product_description = $("#product_description").val();
               var product_category = $("#product_category").val();
               var item_price = $("#item_price").val();

               var file = fileUploadControl.files[0];
               var name = $('#product_image').val().replace(/.*(\/|\\)/, '');

               var file_ext = name.substr(name.lastIndexOf('.')+1);
               var alowed_ext = ['jpg','gif','png'];

               var fileSize = file.size;

               if(product_title.length > 0 && product_description.length > 0 && item_price.length > 0){

                   if(fileSize > 200000){
                       toastr.warning("Invalid image format or image size more than 200kb!");
                   }else{

                       if($.inArray(file_ext, alowed_ext) != -1){
                           var user = Parse.User.current();
                            if (fileUploadControl.files.length > 0 ) {

                                if(product_category != "Select Category"){

                                    waitingDialog.show('Posting item');

                                      var parseFile = new Parse.File(name, file);

                                      parseFile.save().then(function() {
                                       /* The file has been saved to Parse.*/
                                       toastr.info('File Uploaded!');

                                       /*Associate with the product data*/
                                       var product = new Parse.Object("Product");
                                       product.set("product_title", product_title);
                                       product.set("product_description", product_description);
                                       product.set("product_price", item_price);
                                       product.set("product_category", product_category);

                                       var currentUser = Parse.User.current();
                                       product.set("product_seller_location", currentUser.get('residence'));
                                       product.set("product_image", parseFile);
                                       product.set("user", currentUser);

                                       /*read write permissions*/
                                       var postACL = new Parse.ACL(currentUser);
                                       postACL.setPublicReadAccess(true);
                                       postACL.setPublicWriteAccess(false);
                                       product.setACL(postACL);
                                       $('#sell').attr('disabled','true');
                                       product.save(null, {
                                        success: function(Products) {
                                            waitingDialog.hide();
                                            $("#sell").html('<span class="mdi-action-done-all"></span>');
                                            toastr.success(product_title+ ' posted successfully!!!');
                                            $("#add-new").slideToggle();
                                            $('#sell').attr('disabled','false');
                                        },
                                        error: function(Products, error) {
                                            toastr.error('Error Posting item!!');
                                            waitingDialog.hide();
                                            $('#sell').attr('disabled','false');
                                        }
                                     });
                                    }, function(error) {
                                      /*The file either could not be read, or could not be saved to Parse.*/
                                      toastr.error('Error saving the file');
                                      waitingDialog.hide();
                                    });

                                }else{
                                    toastr.warning("Please select category before posting");
                                }
                            }else{
                              toastr.warning("Please include an image");
                              $("#sell").html('Post');
                           }
                       }else{
                        toastr.warning("File/Image format not allowed! Upload jpg, png of gif");
                        $("#sell").html('Retry...');
                       }
                   }
               }else{
                  toastr.warning("Please fill all item details before posting");
                  $("#sell").html('Retry');
               }
            }
           }else{
               toastr.warning("Phone Number required, Please edit your profile to set all details before posting items");
           }
        }else{
            toastr.warning("Please login/sign up to post items");
            
            $("#check_login").html("Please login/sign up to post items"
                                   +"<p><a href='http://swap254.com/login'>Login</a></p>"
                                   +"<p><a href='http://swap254.com/register'>Create Account</a></p>");
        }
    });
});