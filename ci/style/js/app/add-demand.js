$(document).ready(function(){

   $("#post-demand").click(function(){
       
       var currentUser = Parse.User.current();
        if (currentUser) {
           $("#post-demand").html('one moment...');

           var currentUser = Parse.User.current();
           if(currentUser.get('user_phone_number') == ''){
                toastr.warning("Please edit your profile fully to set all details before positng");
           }else{

               /*post var(s)*/
               var demand_item_title = $("#demand_item_title").val();
               var demand_item_description = $("#demand_item_description").val();
               var demand_item_category = $("#demand_item_category").val();
               var demand_item_price = $("#demand_item_price").val();

               if(demand_item_category != "Select Category"){

                   var product = new Parse.Object("New_Product_Demands");
                   product.set("title", demand_item_title);
                   product.set("description", demand_item_description);
                   product.set("price", demand_item_price);
                   product.set("category", demand_item_category);

                   var currentUser = Parse.User.current();
                   product.set("user_location", currentUser.get('residence'));
                   product.set("user", currentUser);

                   /*read write permissions*/
                   var postACL = new Parse.ACL(currentUser);
                   postACL.setPublicReadAccess(true);
                   postACL.setPublicWriteAccess(false);
                   product.setACL(postACL);

                   product.save(null, {
                    success: function(demands) {
                        $("#post-demand").html('<span class="mdi-action-done-all"></span>');
                        toastr.success('Item successfully posted, please wait for a seller');
                        //$("#add-new-demand").slideToggle();
                    },
                    error: function(demands, error) {
                        if(error.code == 100){
                            toastr.error('Internet Connection Error');
                            $("#post-demand").html("Error");
                        }else{
                            toastr.error('Error Posting Item...please try refreshing page then try posting again');
                        }
                    }
                 });
             }else{
                toastr.warning("Please select category before posting");
             }
           }
       } else {
            toastr.warning("Please login/sign up to start posting!!");
            bootbox.confirm("Want to Login/Create account", function(result) {
            if(result == true){
              check_login();
            }
          });
        }
    });
});