/**
* UserController
*/
module.controller('UserController',function($scope, $http) {
    
    
    /*logout*/
    $("#logout").click(function(){
        Parse.User.logOut();
        window.location.replace("http://swap254.com");
    });
    
    toastr.options.preventDuplicates = true;

    /*get logged in user data*/
    var currentUser = Parse.User.current();
    if (currentUser) {
        $scope.username = currentUser.get('username');
        $scope.user_phone_number = currentUser.get('user_phone_number');
        $scope.firstname = currentUser.get('firstname');
        $scope.lastname = currentUser.get('lastname');
        $scope.email = currentUser.get('email');
        $scope.residence = currentUser.get('residence');
        $scope.new_notification = currentUser.get('new_notification');
        
        if(currentUser.get("emailVerified") == false){
            $scope.email_varified = "  (Please varify your email for account confirmation in your mailbox)";
            /*$("#resend_email_var").addClass("btn btn-info");
            $("#resend_email_var").html("Resend Email");*/
        }else{
            $scope.email_varified = " ";
        }
        
        if(currentUser.get('firstname') == '' || currentUser.get('lastname') == ''){
            $scope.name_empty = "  (Please edit profile to add your full name)";
            /*$("#resend_email_var").addClass("btn btn-info");
            $("#resend_email_var").html("Resend Email");*/
        }else{
            $scope.name_empty = " ";
        }
        
        if(currentUser.get('user_phone_number') == ''){
            $scope.phone_number = "  (Please edit profile to addd phone number)";
        }else{
            $scope.phone_number = " ";
        }
        
       /*edit profile*/
       $("#business-description-div").hide();
       $("#account-type").change(function(){
           if($("#account-type").val() == "business"){
               $("#business-description-div").slideDown();
           }else{
               $("#business-description-div").hide();
           }
       });
       $("#edit-profile-now").click(function(){
           currentUser.set("username", $("#new-username").val());
           currentUser.set("firstname", $("#new-firstname").val());
           currentUser.set("lastname", $("#new-lastname").val());
           currentUser.set("user_phone_number", $("#user_phone_number").val());
           currentUser.set("password", $("#new-password").val());
           currentUser.set("residence", $("#new-residence").val());
           currentUser.set("acount_type", $("#account-type").val());
           currentUser.set("business-description", $("#business-description").val());
           
           /*make sure fields are filled*/
           if($("#new-password").val()){
               waitingDialog.show('Saving Changes..');
                currentUser.save(null, {
                  success: function(edit_profile) {
                    toastr.success('Profile edited!!');
                    location.reload();
                  },
                  error: function(edit_profile, error) {
                      if(error.code == 209){
                          check_login();
                      }
                      if(error.code == 100){
                          toastr.error("Connection error...please check internent connection and retry");
                          waitingDialog.hide();
                      }else{
                        toastr.error('Error saving changes..please try back later!!');
                        waitingDialog.hide();
                      }
                      waitingDialog.hide();
                  }
                });   
           }else{
               waitingDialog.hide();
               toastr.warning('Please fill all fields before trying to save!');
           }
        });

    } else {
        /*if not logged in*/
        toastr.warning("Redirecting to login");
        setTimeout(function(){
            check_login();
        }, 1500);
    }
});

/*sign up*/
$(document).ready(function(){
    $("#sign-up").click(function(){
       var user = new Parse.User();
        
        var name = $("#username").val();
        var email = $("#email").val();
        var residence = $("#residence").val();
        var password = $("#password").val();
        
        if(residence != "Select your residence" && password != ""){
            if(password != ""){
                user.set("username", name);
                user.set("email", email);
                user.set("residence", residence);
                user.set("password", password);
                
                waitingDialog.show('Creating Account...');
                user.signUp(null, {
                  success: function(user) {
                    /*success registration.*/
                    waitingDialog.hide();
                    toastr.success("Sign Up successful! Redirecting to login...");
                    window.location.replace("http://swap254.com/login");
                  },
                  error: function(user, error) {
                    waitingDialog.hide();
                    /*Show the error message*/
                    if(error.code == 100){
                        toastr.error("Please check your internet connection and provide a password");
                    }else{
                        toastr.warning("Sorry, an error occured, we'll reload the page to fix this");
                        Parse.User.logOut();
                        setTimeout(function(){
                            window.location.reload();
                        }, 1500);
                    }
                  }
                });
            }else{
                toastr.warning("Please create your prefered password");
            }
        }else{
            toastr.warning("please select location, fill fields");   
        }
    });
});

/*sign in*/
$(document).ready(function(){
    
    /*hide password reset field*/
    $("#reset_password").hide();
    $("#progress_bar").hide();
    
    $("#sign-in").click(function(){
       $("#progress_bar").fadeIn("slow");
       var login_user = new Parse.User();

       var login_user_username = $("#username").val();
       var login_user_password = $("#password").val();

        Parse.User.logIn(login_user_username, login_user_password, {
          success: function(login_user) {
            /*successful login.*/
            $("#response").html("Successfully logged in!");
            toastr.success("Successfully logged in!");
            $("#progress_bar").fadeOut("slow");
            window.location.replace("http://swap254.com");
          },
          error: function(login_user, error) {
            if(error.code == 100){
                toastr.error("Please check your internet connection and retry");  
            }else{
                $("#response").html("");
                toastr.warning("Please check your login details and retry");
            }
            $("#progress_bar").fadeOut("slow");
          }
        });
    });

    /*User Reset Password*/
    $("#forgot-password").click(function(){
        $("#response").html("");
        $("#reset_password").slideToggle();
        
        $("#reset_now").click(function(){
            waitingDialog.show('Sending email');
            if($("#reset_password_email").val().length > 0){
                Parse.User.requestPasswordReset($("#reset_password_email").val(), {
                  success: function() {
                       /*Password reset request was sent successfully*/
                      $("#progress_bar").fadeOut("slow");
                      toastr.success('Email sent, please go to your email inbox to reset your password');
                      $("#reset_password").slideToggle();
                      waitingDialog.hide();
                  },
                  error: function(error) {
                    /*Show the error message*/
                    waitingDialog.hide();
                    if(error.code == 100){
                        toastr.error("Conection lost....");
                        
                        setTimeout(function(){
                            toastr.info("Check your internet connection settings and retry"); 
                        }, 1200);
                    }else{
                        Parse.User.logOut();
                        check_login();
                    }
                    $("#reset_password").slideToggle();
                  }
                });
            }else{
                waitingDialog.hide();
                toastr.options.preventDuplicates = true;
                toastr.error("Please enter your email to reset password");
            }
        });
    });
});


/*Resend email varification*/
$("#resend_email_var").click(function(){
   Parse.User.current().set("email", Parse.User.current().get("email"));
   Parse.User.current().save();
});
