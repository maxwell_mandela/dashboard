/**
* App.js
* Material init
* Parse init
* angular init
*/

/*material init*/
$.material.init();
$.material.ripples();

/*initialize parse*/
Parse.initialize("GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3", "GgtZidl1lSXbDBWQKhif8glrjdJVTv8Z1I7ZwgED");

$(document).ready(function(){
  
  /*show errors*/
  $("#connection-console").hide();
  
  /*stick elements*/
  $("#sticker").sticky({topSpacing:0});

  /*add div*/
  $("#add-new").hide();
  $("#show-add-div").click(function(){
    var currentUser = Parse.User.current();
       if (currentUser) {
          $("#add-new").slideToggle();
       } else {
          toastr.warning("Please login/sign up to start posting!!");
         
          bootbox.confirm("Want to Login/Create account?", function(result) {
            if(result == true){
              check_login();
            }
          });
      }
  });
  
    var currentUser = Parse.User.current();
     if (currentUser) {
       $("#is-logged-in").hide();
     } else {
       $("#is-logged-in").show();
     }
});

/*image protect*/
$('img').each(function(){
  $(this)[0].oncontextmenu = function(){
    toastr.options.preventDuplicates = true;
    toastr.error('We\'re sorry. The images are protected!');
    return false;
  };
});


/*angular module*/
var module = angular.module('app', ['angular-loading-bar', 'angularUtils.directives.dirPagination','truncate']);

/*toastr options*/
toastr.options.preventDuplicates = true;

/*parse check login*/
function check_login() {
  if(Parse.User.current()){
  }else{
    Parse.User.logOut();
    window.location.replace("http://swap254.com/login");
  }
}