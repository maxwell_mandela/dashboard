/**
* item view
*/
function getItemData(item_id){
    module.controller('ItemDataController',function($scope, $http) {
        
        /*get item data*/
        $scope.items = [];
        var whereQuery = {objectId: item_id};
         $http({method : 'GET',url : 'https://api.parse.com/1/classes/Product',
            headers: { 'X-Parse-Application-Id':'GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3',
                      'X-Parse-REST-API-Key':'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw'},
            params:  { 
                 where: whereQuery,
                 include:"user",
                 limit: 1
              }
           })
        .success(function(data, status) {
            $scope.items = data.results;
             console.log(data);
            if(data.results.length < 1){
                toastr.error('No item with that id found!');
            }
         })
        .error(function(data, status) {
            toastr.error("Error getting item information");
        })
    });
}