/**
* user profiles view
*/
function getUser(user_name){
    var currentUser = Parse.User.current();
    if (currentUser) {
        currentUser = Parse.User.current();

        module.controller('UsersProfileController',function($scope, $http) {
            var viewUser = user_name;
            
            var currentUser = Parse.User.current();
            if(viewUser == currentUser.get('username')){
                window.location.href = "http://swap254.com/user/myprofile";
            }

            /*get user data*/
            $scope.items = [];
            var whereQuery = {username: viewUser};
             $http({method : 'GET',url : 'https://api.parse.com/1/classes/_User',
                headers: { 'X-Parse-Application-Id':'GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3',
                          'X-Parse-REST-API-Key':'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw'},
                params:  { 
                     where: whereQuery,
                     limit: 1
                  }
               })
            .success(function(data, status) {
                 $("#loading-user").fadeOut("slow");
                $scope.items = data.results;
                if(data.results.length < 1){
                    toastr.error('No users with that username found!');
                }
             })
            .error(function(data, status) {
                 $("#loading-user").html("Error getting user information, please check your internet settings");
                toastr.error("Error getting user information");
            })
        });
    }else{
        toastr.warning("You can't view a member's Card without an account");
        $("#error_msg").html("Please login/sign up to view Cards"
                                   +"<p><a href='http://swap254.com/login'>Login</a></p>"
                                   +"<p><a href='http://swap254.com/register'>Create Account</a></p>");
    }
}