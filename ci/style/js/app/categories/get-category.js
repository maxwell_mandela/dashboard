/**
* categories data factory
*/
function getCategoryItems(cat){
  module.factory('CategoriesdataFactory', function($http) {
      var CategoriesdataFactory = {};
      CategoriesdataFactory.getCategories = function (cat, new_location) {
        var whereQuery = {product_category:cat, product_seller_location: new_location};
        return $http({method : 'GET',url : 'https://api.parse.com/1/classes/Product',
          headers: { 'X-Parse-Application-Id':'GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3',
                    'X-Parse-REST-API-Key':'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw'},
          params:  { 
              include: 'user',
              where: whereQuery,
              limit: 20,
              order: '-createdAt'
            }
         })
      };
      return CategoriesdataFactory;
  });

  /**
  * MainController
  */
  module.controller('CategoriesController',function($scope, $http, $interval, CategoriesdataFactory) {
      $('#show_no_cposts').hide();
    
      /*pagination*/
      $scope.currentPage = 1;
      $scope.pageSize = 20;

      /*final items scope*/
      $scope.items = [];

      /**
      * default location
      * check localstorage
      * if emppty set to tea Farm
      */
      if(localStorage.getItem("new_location")){
        var new_location = localStorage.getItem("new_location");
      }else{
        var new_location = "Tea Farm";
      }
      $scope.saved_location = "Location: "+new_location;

      //var url = window.location.href;
      //var parts = url.split("/");
      //var cat = parts[6];
      //localStorage.setItem("cat", cat);

      getCategories(cat, new_location);

      /*if user changes location*/  
      $scope.changedLocation = function(){
          var new_location = $scope.selectedLocation.value;
          localStorage.setItem("new_location", new_location);
          //var cat = localStorage.getItem("cat");
          getCategories(cat, new_location);
      }

      /*locations*/
      $scope.locations = [
        {name: 'Choose Location', value: 'Tea Farm' }, 
        {name: 'Tea Farm', value: 'Tea Farm' }, 
        {name: 'Kapmaso', value: 'Kapmaso'},
        {name: 'Chepnyogaa', value: 'Chepnyogaa'},
        {name: 'Judea', value: 'Judea'},
        {name: 'Kapcheluch', value: 'Kapcheluch'},
        {name: 'Kabianga Market', value: 'Kabianga Market'},
        {name: 'KMS', value: 'KMS'},
        {name: 'MaryLand', value: 'MaryLand'},
        {name: 'Keter', value: 'Keter'},
        {name: 'Destiny', value: 'Destiny'},
        {name: 'Sawa Hostels', value: 'Sawa Hostels'},
        {name: 'Landmark', value: 'Landmark'},
        {name: 'Ebenezer', value: 'Ebenezer'},
        {name: 'Oasis', value: 'Oasis'},
        {name: 'MotherLand', value:'MotherLand'},
        {name: 'Hostel 1', value:'Hostel 1'},
        {name: 'Hostel 4', value:'Hostel 4'},
        {name: 'Hostel 6', value:'Hostel 6'},
        {name: 'Hostel 7', value:'Hostel 7'}
      ];
      $scope.selectedLocation = $scope.locations[0];

      /*categories*/
      $scope.categories = [
        { name: 'Phones/Tablets', value: 'Phones' }, 
        { name: 'Tea Factory', value: 'tea_factory' }, 
        { name: 'Kapmaso', value: 'kapmaso'}
      ];
      $scope.selectedCategory = $scope.categories[0];

      /*the http*/
      function getCategories(cat, new_location) {
        CategoriesdataFactory.getCategories(cat, new_location)
          .success(function (data, status, headers, config) {
              $scope.items = data.results;
              if($scope.items.length < 1){
                toastr.info("There are no items from this location yet, try changing location and category");
                $('#show_no_cposts').show();
                }else{
                  $('#show_no_cposts').hide();
              }
          })
          .error(function (data, status, headers, config) {
            toastr.error('Failed to Connect to server, please check your internet settings');
        })
        }

      /**
      * view product details
      */
      $scope.CategoriesgetItemDetails = function(index){
          var activeItem = $scope.items[index];
          CategoriesdataFactory.activeItem = activeItem;

          //var getUrl = window.location;
          //var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
          //window.location.href= baseUrl+"/swap/view_item/"+activeItem.objectId;
          window.location.href= "http://swap254.com/swap/view_item/"+activeItem.objectId;
      }
  });

  function CategoriesPagingController($scope) {
      $scope.CategoriespageChangeHandler = function(num) {
    };
  }

  module.controller('CategoriesPagingController', CategoriesPagingController);
}