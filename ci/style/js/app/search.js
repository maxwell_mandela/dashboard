module.controller('SearchController',function($scope, $http) {
    $scope.items = [];
    
    $scope.search = function() {
       var search_string = $("#search_string").val();
       var whereQuery = {"$or":[{"product_title":{"$regex": search_string}},{"product_decription":{"$regex": search_string}}]};
        $("#search-btn").html('Searching...');
        
        $http({method : 'GET',url : 'https://api.parse.com/1/classes/Product',
            headers: { 'X-Parse-Application-Id':'GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3',
                      'X-Parse-REST-API-Key':'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw'},
            params:  { 
                 where: whereQuery,
                 limit: 20
              }
           })
        .success(function(data, status) {
            $scope.items = data.results;
            $("#search-btn").html('Done');
         })
        .error(function(data, status) {
            $("#search-btn").html("Error");
        })
    }
});
