$(document).ready(function(){  
    
   /*file input style*/
   $('input[type=file]').bootstrapFileInput();
   $('.file-inputs').bootstrapFileInput();

    /*DOM MANIP image preview*/
    $(function () {
        $("#business_poster").change(function () {
            if($("#business_poster")[0].files.length > 4){
                alert('too may files, try maximum of 4!');
            }else{
                $("#dvPreview").empty();
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                //img.attr("style", "height:100px;width: 100px");
                                img.addClass('img-responsive');
                                img.addClass('img-rounded');
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            $("#loading_preview").html(" ");
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        });
    });
    
    function checkImageProperties(the_file){
        var img_status;
        var fileUploadControl = $(the_file)[0];
        var file = fileUploadControl.files[0];
        var name = $(the_file).val().replace(/.*(\/|\\)/, '');
        var file_ext = name.substr(name.lastIndexOf('.')+1);
        var alowed_ext = ['jpg','gif','png'];
        var fileSize = file.size;
        if(fileSize < 200000 && $.inArray(file_ext, alowed_ext) != -1 && fileUploadControl.files.length > 0){
            console.log(name+ " type and size accepted");
            img_status = 1;
            return img_status;
        }else{
            toastr.warning("Error: " +name+ " image you've selected is either not an image file type of size greater 200kb!");
            img_status = 0;
            return img_status;
        }
    }
    
    $("#add_business_now").click(function(){
       var currentUser = Parse.User.current();
       var business_contacts = [];
       if (currentUser){
           
        if (currentUser.get("user_phone_number")){
            
        var fileUploadControl = $("#business_poster")[0];
        if (checkImageProperties("#business_poster") == 1) {
          var file = fileUploadControl.files[0];
          var name = file.name;

          var front_image = new Parse.File(name, file);

          waitingDialog.show('Posting item');
          toastr.info("Uploading files...");

          front_image.save().then(function() {
              var product = new Parse.Object("Businesses");
              product.set("name", $("#business_name").val());
              product.set("description", $("#business_description").val());
              product.set("type", $("#business_category").val());
              product.set("business_location", $("#business_location").val());
              
              //contacts
              var the_features = $("#business_contacts").val().split('\n');
              for (var i in the_features) {
                the_features[i] = the_features[i].trim();
                business_contacts.push(the_features[i]);
              }
              product.set("contacts", business_contacts);
              product.set("poster", front_image);

               var currentUser = Parse.User.current();
               product.set("added_by", currentUser);

               var postACL = new Parse.ACL(currentUser);
               postACL.setPublicReadAccess(true);
               postACL.setPublicWriteAccess(false);
               product.setACL(postACL);
               
               var business_name = $("#business_name").val();
               var business_description = $("#business_description").val();
              
               if(business_name.length > 0 && business_description.length > 0 && $("#business_category").val().length > 0 && $("#business_contacts").val().length > 0){
                   product.save(null, {
                    success: function(Businesses) {
                        waitingDialog.hide();
                        $("#add_business_now").html('<span class="mdi-action-done-all"></span>');
                        toastr.success(product_title+ ' posted successfully!!!');
                    },
                    error: function(Businesses, error) {
                        toastr.error('Error Posting item!!');
                        waitingDialog.hide();
                        if(error.code == 209){
                            toastr.warning("Hi, we ran into a session error");
                            toastr.info("Please do log out and login again to corrrect this");
        
                              bootbox.confirm("Want to proceed to login then retry??", function(result) {
                                if(result == true){
                                  Parse.User.logOut();
                                  window.location.replace("http://localhost/dashboard/login");
                                }
                              });
                        }
                    }
                 });
               }else{
                   toastr.warning("Please fill all house details before posting!!");
                    waitingDialog.hide();
                }
              }, function(error) {
              /*The file either could not be read, or could not be saved to Parse.*/
              toastr.error('Error saving the file');
              waitingDialog.hide();
            });
           }
          }
        }
    });
    
});