var query = new Parse.Query("Product");
query.descending("createdAt");
query.include("user");
query.equalTo("user", Parse.User.current());
query.find({
  
  success: function(results) {
    var seller_id = " ";

    for (var i = 0; i < results.length; i++) {
      var object = results[i];
      var pic = object.get("product_image");
      var the_img = pic.url();
      var user = object.get("user");
      var seller_id = user.id;
      var object_id = object.id;
      var sold = '';
      var new_btn_text = '';
        
      if(object.get("sold") == true){
          new_btn_text = "Unmark Sold";
          var sold = false;
          var feedback = " Unmarked Sold";
      }else{
          new_btn_text = "Mark as sold";
          var sold = true;
          var feedback = " Marked as sold";
      }
      
      $('#mproductDetail').append('<div class="item cursor-hand col-xs-36 list-group-item">'
            +'<div class="thumbnail">'
                +'<img class="group list-group-image" src='+the_img+' alt="" />'
                +'<div class="caption">'
                    +'<h4 class="group inner list-group-item-heading">'+object.get('product_title')
                        +'<span class="lead text text-info">Ksh. '+object.get('product_price')+'</span></h4>'
                    +'<p class="group inner list-group-item-text">'+object.get('product_description')+'</p>'
                    +'<div class="group row">'
                        +'<div class="col-sm-20 col-xs-20">'
                            +'<span>Category: <a href="#">'+object.get('product_category')+'</a></span>'
                            +'<div>Edit: '
                                  +'<span class="btn btn-info btn-sm" id="marksold">'+new_btn_text+'</span>'
                                  //+'<span class="btn btn-edit btn-sm" id="edit">Edit</span>'
                                  +'<span class="btn btn-delete btn-sm" id="delete">Delete</span>'
                            +'</div>'
                        +'</div>'
                        +'<p><span class="" id="object_id" style="visibility:hidden;">'+object_id+'</span></p>'
                    +'</div>'
                +'</div>'
            +'</div>'
        +'</div>');
        
        $('#mproductDetailxs').append('<div class="item cursor-hand col-xs-36 grid-group-item">'
            +'<div class="thumbnail">'
                +'<img class="group grid-group-image" src='+the_img+' alt="" />'
                +'<div class="caption">'
                    +'<h4 class="group inner list-group-item-heading">'+object.get('product_title')
                        +'<span class="lead text text-info">Ksh. '+object.get('product_price')+'</span></h4>'
                    +'<p class="group inner list-group-item-text">'+object.get('product_description')+'</p>'
                    +'<div class="group row">'
                        +'<div class="col-sm-20 col-xs-20">'
                            +'<span>Category: <a href="#">'+object.get('product_category')+'</a></span>'
                            +'<div>Edit: '
                                  +'<span class="btn btn-info btn-sm" id="marksold">'+new_btn_text+'</span>'
                                  //+'<span class="btn btn-edit btn-sm" id="edit">Edit</span>'
                                  +'<span class="btn btn-delete btn-sm" id="delete">Delete</span>'
                            +'</div>'
                        +'</div>'
                        +'<p><span class="" id="object_id" style="visibility:hidden;">'+object_id+'</span></p>'
                    +'</div>'
                +'</div>'
            +'</div>'
        +'</div>');
        
        /*mark sold*/
        $("#marksold").click(function(){
            waitingDialog.show('Hold on a minute');
            var this_product_id = $("#object_id").text();
            object.set("sold", sold);
            object.save(null, {
            success: function(Products) {
                waitingDialog.hide();
                toastr.success(object.get('product_title')+ feedback);
            },
            error: function(Products, error) {
                toastr.error('Error while processing request, please try again later');
                waitingDialog.hide();
            }
         });
        });
        
        /*edit*/
        $("#edit").click(function(){
            /*go to edit page*/
            //window.location.href = "http://localhost/dashboard/swap/edit_item/"+seller_id+"/"+object_id;
        });
        
        /*delete*/
        $("#delete").click(function(){
            bootbox.confirm("You will permanently remove this item from any list of products, do you really want to do this?", function(result) {
            if(result == true){
              /*set value as deleted*/
                waitingDialog.show('Hold on a minute');
                var this_product_id = $("#object_id").text();
                object.set("trash", true);
                object.save(null, {
                success: function(Products) {
                    waitingDialog.hide();
                    toastr.success(object.get('product_title')+ ' Deleted');
                },
                error: function(Products, error) {
                    toastr.error('Error while processing request, please try again later');
                    waitingDialog.hide();
                }
             });
            }
          });
        });
    }
  },
  error: function(error) {
    toastr.error("An error occured while fetching items, please refresh");
  }
});

function markSold(sold_item_id){
    var id = sold_item_id;
    alert(id);
}