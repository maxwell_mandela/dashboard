/**
* categories data factory
*/
function getDemandedItemsList(cat){
  module.factory('getDemandedItemsdataFactory', function($http) {
      var getDemandedItemsdataFactory = {};
      getDemandedItemsdataFactory.getDemandedItems = function (cat) {
        var whereQuery = {category:cat};
        return $http({method : 'GET',url : 'https://api.parse.com/1/classes/New_Product_Demands',
          headers: { 'X-Parse-Application-Id':'GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3',
                    'X-Parse-REST-API-Key':'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw'},
          params:  { 
              include: 'user',
              where: whereQuery,
              limit: 12,
              order: '-createdAt'
            }
         })
      };
      return getDemandedItemsdataFactory;
  });

  /**
  * MainController
  */
  module.controller('getDemandedItemsController',function($scope, $http, $interval, getDemandedItemsdataFactory) {
      /*pagination*/
      $scope.currentPage = 1;
      $scope.pageSize = 20;

      /*final items scope*/
      $scope.items = [];

      getDemandedItems(cat);

      /*the http*/
      function getDemandedItems(cat) {
        getDemandedItemsdataFactory.getDemandedItems(cat)
          .success(function (data, status, headers, config) {
              $scope.items = data.results;
              if($scope.items.length < 1){
                toastr.info("There are no new item demands yet");
              }
          })
          .error(function (data, status, headers, config) {
            toastr.error('Failed to Connect to server, please check your internet settings');
        })
        }
  });
}