<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
$(document).ready(function(){
    var maxLength = 300;
    $(".show-read-more").each(function(){
        var myStr = $(this).text();
        if($.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(newStr);
            $(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>');
            $(this).append('<span class="more-text">'+removedStr+'</span>');
        }
    });
    $(".read-more").click(function(){
        $(this).siblings(".more-text").contents().unwrap();
        $(this).remove();
    });
});
</script>

<style type="text/css">
    .show-read-more .more-text{
        display: none;
    }
</style>

<body class="container">
    
    <!-- the notice board -->
    <div class="col-sm-18 col-md-offset-8">
        <?php
            $data = file_get_contents('http://localhost/wordpress/wp-json/posts/');
            $data = json_decode($data);
            foreach($data as $post){
            ?>
            <div class="the-content card card-nb">
              <div class="image">
                  <a href="<?php echo base_url('blog/read_post')."/".$post->slug; ?>">
                    <?php 
                        if(strlen($post->featured_image->guid) > 0){
                            print "<img src='".$post->featured_image->guid."' class='img-responsive image'>";
                        }
                    ?>
                  </a>
              </div>
              <div class="content">
                  <span class="title">
                      <h3>
                          <a href="<?php echo base_url('blog/read_post')."/".$post->slug; ?>">
                              <?php print $post->title; ?></a></h3></span>
                  <div class="show-read-more" style="font-family: 'Dosis',sans-serif; font-size: 1.2em; font-weight:500; color:#363744;">
                      <?php print $post->content; ?></div>
              </div>
            </div>
        <?php
        }
        ?>
        <p id="load" class="btn btn-info">load more</p>
        
        <script>
            /**
            * Load more
            */
            $('div.the-content').hide();
            $('div.the-content').slice(0, 2).show(); // select the first
            if($('div.the-content').length > 2){
            }else{
               $("#load").hide();
            }
            $("#load").click(function(e){ // click event for load more
                e.preventDefault();
                $('div.the-content:hidden').slice(0, 2).slideDown("slow"); // select next hidden divs and show them
                if($('div.the-content:hidden').length == 0){ // check if any hidden divs still exist
                    $("#load").hide();
                }
            });
        </script>
    </div>
</body>
</html>