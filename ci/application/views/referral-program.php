<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">
    <div class="jumbotron">
        <h1><span class="text-info">Swap254</span> Referral Program</h1>
        <small>Make money inviting your friends to buy and sell from Swap254!</small>
    </div>
    
    <div class="col-sm-21 col-md-25 bg-white">
        <h2>How To</h2>
        <p><a href="#">Opt into the referral program</a> and all get the following in one package!</p>
        <div class="row">
            <div class="col-sm-36">
                <h3>Cash Reward: <span class="text text-danger">Ksh.500</span></h3></div>
            
            <div class="col-sm-36">
                <h3>Free customized shop set up.</h3>
                <p>Tell us how you would like your onlince store to look, design it with your own logos and colors</p>
                <p>Free Branding will help your customers identify well with your products</p>
                <p>Contact us if you need delivery services to your customers</p>
            </div>
            
            <div class="col-sm-18">
                <h3>Free Marketting for your shop products</h3>
                <p>Reach your customers easily with our free residential targeted ads.</p>
                <p>You will get upto 6 months of free ads for your items in all residences around UoK</p>
            </div>
            
            <div class="col-sm-18">
                <h3>Free Market research for what is selling and what is not</h3>
                <p>Swap254 knows what sells and what doesn't, liase with us to get market trends and keep your business running</p>
            </div>
            
            <div class="col-sm-36">
                <h3>Free Email Marketing to your customers for 6 months</h3>
                <p>Want to send newsletters to your customers? We got you covered, we will handle bulk mailing for you to not more than 1000 customers free for 6 months</p>
            </div>
        </div>
        <p><a href="#">Click here</a> to get your link emailed to you.</p>
    </div>
    
    <div class="col-sm-offset-1 col-md-offset-1 col-sm-14 col-md-10 bg-white">
        <h2>Talk to Us</h2>
        <p>Contact us through either of the following:</p>
        <p>Email: referral-program@swap254.com</p>
        <p>Phone: +254 (0)716 261 703</p>
    </div>
</div>
