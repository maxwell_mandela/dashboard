<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- add new item -->
<div  id="page" class="chat">
    <div class="col-sm-26 col-sm-offset-5 bg-white">

        <h2 class="text text-info">ChatAnonymous</h2>
        <div class="row">
            <div class="col-sm-23">
              <h2 class="text text-info">New Chat</h2>
              <div class="form-group">
                  <input type="text" maxlength="50"  placeholder="Start typing..." class="form-control">
                  <button class="btn btn-info">Send</button>
              </div>
            </div>
            
          <div class="col-sm-13">
              <h2 class="text text-info">Online(452)</h2>
          </div>
        </div>
    </div>
<div class="col-sm-offset-5"></div>