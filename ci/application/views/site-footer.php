<footer class="container the-footer" id="colophon">
        <div class="row">
            
            <div class="col-sm-13">
                <h3>About</h3>
                <p>Swap254 is a Comrade Trade Center for University of Kabianga targeting the diaspora guys who due to long distances to one another will benefit from a central...<?php echo anchor("about", "read more")?></p>
            </div>
            
            <div class="col-sm-14">
                <h3>Contact Us</h3>
                <p>info@swap254.com | team@swap254.com | support@swap254.com</p>
                <p>+0716261703</p>
            </div>
            
            <div class="col-sm-9">
                <h3>Important</h3>
                <p><?php //echo anchor("about/terms_conditions", "Terms and Conditions")?><?php //echo anchor("about/privacy_policy", "Privacy Policy")?></p>
                
                <p><?php echo anchor("about/terms_conditions", "Terms and Conditions")?> | Privacy Policy</p>
                <p>©2015 | All rights reserved</p>
            </div>
        </div>
</footer>
    
    <script>
        $(document).ready(function() {
          var bodyHeight = $("body").height();
          var vwptHeight = $(window).height();
          if (vwptHeight > bodyHeight) {
            $("footer#colophon").css("position","absolute").css("bottom",0);
          }
        });
    </script>

</body>
</html>