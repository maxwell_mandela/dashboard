<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    body{
        background-color:#056;
        color:#fff;
    }
    .row{
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
    }
    .first-row-content{
        margin:20px;
    }
</style>
    <div class="container">
        <div class="row first-row-content">
            <div class="col-sm-36 text-center">
                <h1>Browser not Compatible!!</h1>
                <p>Dear user, we use complex technologies to deliver this app for the awesome experience it offers, as a result, some
                browsers without the ability to render the pages as intended are not supported but we are working on this
                for backward compatibility. Thank you!</p>
                <p>Opera Browsers will be supported as soon as possible</p>
            </div>
        </div>
    </div>
</body>
</html>