<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
    @import "bourbon";

$color_purple: #5133AB;
$color_red: #AC193D;
$color_orange: #DC572E;
$color_green: #00A600;
$color_blue: #2672EC;

$base_font_size: 14px;
$base_line_height: 1.5em;
$text_font_family: 'Open Sans', "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
$display_font_family: 'Open Sans', "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;

body {
	font-family: $text_font_family;
	font-size: $base_font_size;
	line-height: $base_line_height;
	font-weight: 400;
}

p, span, a, ul, li, button {
	font-family: inherit;
	font-size: inherit;
	font-weight: inherit;
	line-height: inherit;
}

strong {
	font-weight: 600;
}

h1, h2, h3, h4, h5, h6 {
	font-family: $display_font_family;
	line-height: $base_line_height;
	font-weight: 300;	
}

strong {
  font-weight: 400;
}

.tile {  
  width: 100%;
	display: inline-block;
	box-sizing: border-box;
	background: #fff;		
	padding: 20px;
	margin-bottom: 30px;
  
  .title {
		margin-top: 0px;
	}
  
  &.purple, &.blue, &.red, &.orange, &.green {
    color: #fff;
  }
  
  &.purple {
    background: $color_purple;

		&:hover {
			background: darken($color_purple, 10%);
		}		
  }
  
  &.red {
    background: $color_red;

		&:hover {
			background: darken($color_red, 10%);
		}		
  }
  
  &.green {
    background: $color_green;

		&:hover {
			background: darken($color_green, 10%);
		}		
  }
  
  &.blue {
    background: $color_blue;

		&:hover {
			background: darken($color_blue, 10%);
		}	
  }
  
  &.orange {
    background: $color_orange;

		&:hover {
			background: darken($color_orange, 10%);
		}	
  }
}
</style>

  <div class="row">
    <div class="col-md-36">
      <h1><strong>Categories</strong></h1>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-18 col-sm-12">
      <div class="tile purple">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Phones");?>">Phones and Tables</a></h3>
        <img class="img-responsive" src="http://s.tmocache.com/content/dam/tmo/en-g/Holiday%20Q4/custom-lp-marquee-new-years-samsung-sale-v2.png">
        <p>All the phones around campus, you can sell your android for wndows 10!.</p>
      </div>
    </div>
    
    <div class="col-xs-18 col-sm-12">
      <div class="tile red">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Electronics");?>">Electronics/Videos</a></h3>
        <img class="img-responsive" src="https://pmcvariety.files.wordpress.com/2015/06/speaker-pill-2-black-standard-thrqrtleft-o-e1433973672504.png?w=670">
        <p>Music speakers, sub woofers, laptop speakers.</p>
      </div>
    </div>
    
    <div class="col-xs-18 col-sm-12">
      <div class="tile orange">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Fashion");?>">Fashion and Clothes</a></h3>
        <img class="img-responsive" src="http://sheisdallas.com/wp-content/uploads/2009/03/clothes-on-rack-feature.jpg">
        <p>Want to change your closet? Sell old items or buy from your jirani.</p>
      </div>
    </div>
    
    <div class="col-xs-18 col-sm-12">
      <div class="tile orange">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Accommodation");?>">Accomodation/Hostels</a></h3>
        <img class="img-responsive img-rounded" src="<?php echo base_url();?>ci/style/img/hstl.jpg">
        <p>Find Accommodation and hostels</p>
      </div>
    </div>
    
    <div class="col-sm-12">
      <div class="tile green">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Services");?>">Jobs and services</a></h3>
        <p>Post a job or find one, comrades have the best workforce in the country.</p>
      </div>
    </div>
    
    <div class="col-sm-12">
      <div class="tile blue">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Computers");?>">Computers and Accessories</a></h3>
      </div>
    </div>  
    
    <div class="col-sm-12">
      <div class="tile blue">
        <h3 class="title"><a href="<?php echo base_url("swap/categories/Furniture");?>">Furniture</a></h3>
      </div>
    </div>
  </div>