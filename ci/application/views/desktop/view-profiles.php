<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php add_js(
    array('app/view-user.js', 'search.js')
);?>

<script>getUser('<?php print $username ?>');</script>
<body>
    <div id="error_msg"></div>

    <div class="container" ng-controller="UsersProfileController">
        <h1 id="loading-user" class="col-sm-16 col-md-16 col-sm-offset-10 col-md-offset-10">Loading User</h1>

        <div class="col-sm-16 col-md-16 col-sm-offset-10 col-md-offset-10 bg-white" ng-repeat="item in items">
            <h2 class="text text-info">{{item.username}}'s Card</h2>
            <p class="text text-primary">
                <span class="mdi-action-account-circle text text-info"></span>
                {{item.firstname + ' ' + item.lastname}}
            </p>
            <p>
                <span class="mdi-communication-call"></span> {{item.user_phone_number}}
            </p>
            <p id="email"><span class="mdi-content-mail text text-danger"></span> {{item.email}}</p>
            <p><span class="mdi-communication-location-on text text-primary"></span> {{item.residence}}</p>
            <p>
                <span class="mdi-action-schedule text text-info"></span>
                Member Since {{item.createdAt | date : format : timezone }}
            </p>
        </div>   
    </div>
    <div id="cover"><h1>Loading User</h1></div>

    <script>
        $(window).on('load', function() {
            $("#cover").fadeOut("slow");
        });
    </script>
    
</body>
</html>