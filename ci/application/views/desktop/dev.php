

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
/**
* ItemDetaildata factory
*/
var query = new Parse.Query("Product");
query.equalTo("objectId", "<?php print $objectId;?>");
query.include("Product.user");
query.find({
    success: function(results) {
      for(i in results){
        console.log(results[i].get("user").get("username"));
      }
    },
    error: function(error) {
       console.error("Error: " + error.code + " " + error.message);
    }
});
</script>

<div id="cover"><h1>Fetching item</h1></div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
</script>
        
</body>
</html>