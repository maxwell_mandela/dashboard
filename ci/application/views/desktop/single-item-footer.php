<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- load js -->
<?php add_js(
    array('jquery.js',
           'bootstrap.min.js',
           'material.min.js',
           'ripples.min.js',
           'jquery.sticky.js',
           'parse-1.5.0.min.js',
           'bootstrap.file-input.js',
           'bootstrap-hover-dropdown.min.js',
           'angular.min.js',
           'loading-bar.js',
           'dirPagination.js',
           'app/app.js',
           'toastr.js',
           'truncate.js')
    )
?>
<!-- get benchmark -->
<?php get_benchmark(false);?>