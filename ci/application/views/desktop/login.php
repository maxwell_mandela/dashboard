<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body style="background-color:#056;">
    <div class="col-md-10"></div>
    <div class="col-md-16 bg-white" style="margin-top:10%;">
        <h2 class="text text-info text-center">Sign In</h2>
        
        <div class="row">
            <div class="col-sm-12 col-sm-offset-12">
                <img class="img-responsive" src="<?php echo base_url();?>ci/style/img/logo.jpg">
            </div>
        </div>
        
        <?php $data = array(
            'the_name' => array(
                'name' => 'username',
                'id' => 'username',
                'type' => 'text',
                'placeholder' => 'name',
                'class' => 'form-control'
            ),
            'password' => array(
                'name'        => 'password',
                'id' => 'password',
                'type'        => 'password',
                'placeholder' => 'password',
                'class'       => 'form-control'
            )
        );

        foreach($data as $input_field){
            echo form_input($input_field);
        }
        ?>
        <span id="sign-in" class="btn btn-info">Log In</span>
        
        <span class="text text-info cursor-hand" style="float:right;" id="forgot-password">Forgot Password??</span>
        <?php echo br();?>
        <a class="text text-primary cursor-hand" href="register">Create new account</a>
        <br>
        <span id="response" class="text text-center text-primary"></span>
        <div class="row" id="reset_password">
            <div class="col-sm-36">
                <br><br>
                <input type="email" placeholder="Enter Email" class="form-control" id="reset_password_email">
                <span id="reset_now" class="btn btn-info">Reset</span>
            </div>
        </div><!-- password reset form -->
        
        <div class="row">
            <div class="col-sm-36" id="progress_bar">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">Hold on</div>
                </div>
            </div>
        </div><!-- progress bar -->
    </div>
    <div class="col-md-6"></div>
    
<div id="cover"><h1>Loading Awesome</h1></div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
</script>
    
</body>
</html>