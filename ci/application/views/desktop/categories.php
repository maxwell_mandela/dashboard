<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php add_js(
    array('app/get-demands.js',
          'app/categories/get-category.js'));?>

<div class="row">
    <div class="col-sm-11 col-md-10">
        
        <div class="bg-white">
            <div class="row">
                <div class="col-sm-26 col-sm-offset-5">
                    <img class="img-responsive" src="<?php echo base_url();?>ci/style/img/logo.jpg">
                </div>
            </div>
                
            <div class="row">
                <script>getDemandedItemsList('<?php print $category_name;?>');</script>
                
                <div ng-controller="getDemandedItemsController">
                    <h3 class="text text-info"><?php print $category_name;?> demands</h3>
                    
                    <div id="products-xs">
                        <div class="row grid-group">
                            <div class="item col-xs-36" dir-paginate="item in items | filter:q | itemsPerPage: pageSize" current-page="currentPage">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">{{item.title}}
                                            <span class="item-date">{{item.createdAt | date:'yyyy/MM/dd'}}</span></h4>
                                        <p class="group inner list-group-item-text">{{item.description | words:15}}</p>
                                        <div class="">
                                            <p class="lead text text-info">Ksh. {{item.price}}</p>
                                            <p>Category: <a href="#">{{item.category}}</a>,
                                                <span class="text text-right">
                                                    <span class="mdi-communication-call text-text-success"></span>  
                                                    {{item.user.user_phone_number}}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>

            <script>getCategoryItems('<?php print $category_name;?>');</script>
        
            <div class="bg-white hidden-xs">
                <h2 class="text text-info hidden-xs hidden-sm">Popular Locations</h2>
                <h4 class="text text-info visible-sm">Popular Locations</h4>

                <div class="hidden-xs">
                    <p><span class="mdi-av-new-releasess text text-danger"></span> Tea Farm</p>
                    <p><span class="mdi-maps-local-restaurants text-text-warning"></span> Chepnyogaa</p>
                    <p><span class="mdi-maps-local-restaurants" style="color:purple;"></span> Kabianga</p>
                    <p><span class="mdi-maps-local-restaurants" style="color:purple;"></span>Judea</p>
                </div>
            </div><!-- popular places -->
        </div>
    </div>

    <div class="col-sm-25 col-md-26">
        <div ng-controller="CategoriesController" class="bg-white" style="padding:10;">
            <div class="row">
                <div class="col-sm-18">
                    <h3 class="text text-info">New <?php print $category_name;?></h3> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-15 col-xs-20">
                  <label for="search">Search:</label>
                  <input ng-model="q" class="form-control" placeholder="Type here to search">
                </div>
                <div class="col-sm-13 col-xs-16">
                      <label for="search">Change Location</label>
                      <select class='form-control' 
                        data-ng-model='selectedLocation' 
                        data-ng-options='item as item.name for item in locations' 
                        ng-change="changedLocation()"></select>
                </div>
                
                <div class="col-sm-8 hidden-xs">
                    <div class="btn-group">
                        <a href="#" id="list" class="btn btn-info btn-sm">
                            <span class="mdi-action-view-list"></span>
                        </a>
                        <a href="#" id="grid" class="btn btn-info btn-sm">
                            <span class="mdi-action-view-module"></span>
                        </a>
                    </div>
                </div>
              </div>
            <br>
            
            <div id="show_no_cposts">
                <p>Sorry, we didn't find posts form this category and location combination, please try browsing different locations</p>
                <p>If you'd like to add an item, click the '+' link at the top of the page</p>
            </div>
            
            <div id="products" class="hidden-xs row list-group">
                <div class="item  col-xs-36 col-lg-12" dir-paginate="item in items | filter:q | itemsPerPage: pageSize" current-page="currentPage">
                    <div class="thumbnail cursor-hand" ng-click="CategoriesgetItemDetails($index)">
                        <img class="group list-group-image" src="{{item.product_image.url}}" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">{{item.product_title}}
                                <span class="item-date">{{item.createdAt | date:'yyyy/MM/dd'}}</span></h4>
                            <p class="group inner list-group-item-text">{{item.product_description | words:20}}</p>
                            <div class="row">
                                <div class="col-xs-20 col-sm-20">
                                    <p class="lead text text-info">Ksh. {{item.product_price}}</p>
                                    <p>Category: <a href="#">{{item.product_category}}</a>,
                                        <span class="text text-right">{{item.user.user_phone_number}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="products-xs" class="visible-xs">
                <div class="row grid-group">
                    <div class="item col-xs-36" dir-paginate="item in items | filter:q | itemsPerPage: pageSize" current-page="currentPage">
                        <div class="thumbnail" ng-click="CategoriesgetItemDetails($index)">
                            <img class="group grid-group-image" src="{{item.product_image.url}}" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">{{item.product_title}}
                                    <span class="item-date">{{item.createdAt | date:'yyyy/MM/dd'}}</span></h4>
                                <p class="group inner list-group-item-text">{{item.product_description | words:15}}</p>
                                <div class="">
                                    <p class="lead text text-info">Ksh. {{item.product_price}}</p>
                                    <p>Category: <a href="#">{{item.product_category}}</a>,
                                        <span class="text text-right">
                                            <span class="mdi-communication-call text-text-success"></span>  
                                            {{item.user.user_phone_number}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div ng-controller="CategoriesPagingController" class="other-controller">
              <div class="text-center">
                  <dir-pagination-controls boundary-links="true" on-page-change="CategoriespageChangeHandler(newPageNumber)"  template-url="<?php echo base_url();?>dirPagination.tpl.html">
                  </dir-pagination-controls>
              </div>
            </div>
        </div>
    </div>
</div>

<div id="cover"><h1>Loading ...</h1></div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
</script>

<script>
    $('#products .item').addClass('list-group-item');
    $(document).ready(function() {
        $('#products .item').addClass('list-group-item');
        $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
        $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
    });
    </script>
</body>
</html>