<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php echo doctype();?>

<html lang="en" ng-app="app">
<head>
	<meta charset="utf-8">
    <title>Swap254 | Buy Sell all of UoK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php
        $meta = array(
            array(
                    'name' => 'description',
                    'content' => 'Swap items within University of Kabianga, trade freely, target all residents from Kabianga to Tea Factory. Swap254 is a Comrade Trade Center for University of Kabianga targeting the diaspora guys who due to long distances to one another will benefit from a central online free market system.'
            ),
            array(
                    'name' => 'keywords',
                    'content' => 'Swap all your stuff for new in swap254, get anything you need from a comrade!!!, find all phones on sale from University of Kabianga and sorroudning, now you can see what your next door neighbour is selling!, find food and view menus before walking in to a restourant!, University of Kabianga diaspora students'
            ),
            array(
                    'name' => 'Content-type',
                    'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
            )
    );

    echo meta($meta);
    ?>
    <meta property="og:image" content="http://swap254.com/ci/style/img/logo.jpg"/>
  
    <link rel="stylesheet" href="http://localhost/dashboard/ci/style/mdl/material.min.css">
    <link rel="stylesheet" href="http://localhost/dashboard/ci/style/mdl/material.teal-orange.min.css">
    <script src="http://localhost/dashboard/ci/style/mdl/material.min.js"></script>
      
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

    <!-- Always shows a header, even in smaller screens. -->
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
          <!-- Title -->
          <span class="mdl-layout-title">Swap254</span>
          <!-- Add spacer, to align navigation to the right -->
          <div class="mdl-layout-spacer"></div>
          <!-- Navigation. We hide it in small screens. -->
          <nav class="mdl-navigation mdl-layout--large-screen-only">
            <a class="mdl-navigation__link" href="">About</a>
            <a class="mdl-navigation__link" href="">Contact us</a>
            <a class="mdl-navigation__link" href="">Logout</a>
          </nav>
        </div>
      </header>
        
      <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Swap254</span>
        <nav class="mdl-navigation">
          <a class="mdl-navigation__link" href="">Phones</a>
          <a class="mdl-navigation__link" href="">Computers</a>
          <a class="mdl-navigation__link" href="">Accomodation</a>
          <a class="mdl-navigation__link" href="">Eat Out</a>
          <a class="mdl-navigation__link" href="">Electronics/Video</a>
          
          <hr>
          <a class="mdl-navigation__link" href="">Phones</a>
          <a class="mdl-navigation__link" href="">Computers</a>
          <a class="mdl-navigation__link" href="">Accomodation</a>
          <a class="mdl-navigation__link" href="">Eat Out</a>
          <a class="mdl-navigation__link" href="">Electronics/Video</a>
          
        </nav>
      </div>
      
      <main class="mdl-layout__content">
        <div class="page-content">
          
          <?php
            $to = "mxmandela@gmail.com";
            $subject = "Hi, php email test";
            $message = "hey max";
            $headers = "From: info@swap254.com\r\n";
            $headers .= "Reply-To: no-reply@swap254.com\r\n";
            $headers .= "Return-Path: team@swap254.com\r\n";
            $headers .= "CC: maxwellmandela@swap254.com\r\n";
            $headers .= "BCC: careers@swap254.com\r\n";
            if ( mail($to,$subject,$message,$headers) ) {
               echo "The email has been sent!";
               } else {
               echo "The email has failed!";
               }
            ?>
          
        </div>
      </main>
      
      <footer class="mdl-mini-footer">
        <div class="mdl-mini-footer__left-section">
          <ul class="mdl-mini-footer__link-list">
            <li><a href="#">Help</a></li>
            <li><a href="#">Privacy and Terms</a></li>
            <li><a href="#">User Agreement</a></li>
          </ul>
        </div>
        
        <div class="mdl-mini-footer__right-section">
          <ul class="mdl-mini-footer__link-list">
            <li><a href="#">Contact us</a></li>
            <li><a href="#">Report issues</a></li>
          </ul>
        </div>
      </footer>
      
    </div>