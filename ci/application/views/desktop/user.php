<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php add_js(
    array('app/added.js',
          'app/view-user.js')
    )
?>
<div class="row" ng-controller="UserController" id="user-profile">
      
    <!-- my profile -->
    <div class="col-sm-9">
        <div class="bg-white">
            <h2 class="text text-info">{{username}}</h2>
            <!--<h3 id="name"> {{username}}</h3>-->
            <p class="text text-primary">
                <span class="mdi-action-account-circle text text-info"></span> 
                {{firstname + ' ' + lastname + name_empty}}
            </p>
            <p>
                <span class="mdi-communication-call"></span> {{user_phone_number}}
                <small class="text text-danger">{{' '+phone_number}}</small>
            </p>
            <p id="email">
                <span class="mdi-content-mail text text-danger"></span> {{email}}
                <small class="text text-danger">{{' '+email_varified}}</small>
                <!--<span id='resend_email_var'></span>--></p>
            
            <p><span class="mdi-communication-location-on text text-primary"></span> {{residence}}</p>
            <p class="btn btn-info cursor-hand" id="show-add-div">Edit Profile</p>
        </div>
    </div>
    
     <!-- edit profile -->
    <div id="add-new" class="col-sm-27">
        <div class="bg-white" style="height:85vh;">
            <h3 class="text text-info" style="cursor: hand;">Edit your Profile</h3>
            <input type="text" class="form-control" value="{{username}}" id="new-username">
            <span class="help-block">Your username</span>
            
            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="text" maxlength="50" id="new-firstname" placeholder="First Name" class="form-control" value="{{firstname}}">
                    <span class="help-block">First Name</span>
                </div>
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="text" maxlength="50" id="new-lastname" placeholder="Last Name" class="form-control" value="{{lastname}}">
                    <span class="help-block">Last Name</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="password" class="form-control" placeholder="Password" id="new-password" value="{{password}}">
                    <span class="help-block">Enter your password or create new</span>
                </div>

                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="number" min="10" max="14" class="form-control" placeholder="Your Phone" id="user_phone_number" value="{{user_phone_number}}">
                    <span class="help-block">Change phone number</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <select class="form-control" id="new-residence">
                        <optgroup label="Diaspora">
                            <option value=" ">Choose residence</option>
                            <option value="Tea Farm">Tea Farm</option>
                            <option value="Chepnyogaa">Chepnyogaa</option>
                            <option value="Kapmaso">Kapmaso</option>
                            <option value="Judea">Judea</option>
                            <option value="Kapcheluch">Kapcheluch</option>
                            <option value="Kabianga Market">Kabianga Market</option>
                            <option value="KMS">KMS</option>
                            <option value="MaryLand">Mary Land</option>
                            <option value="Keter">Keter</option>
                            <option value="Destiny">Destiny</option>
                            <option value="Sawa Hostels">Sawa Hostels</option>
                            <option value="Landmark">Landmark</option>
                            <option value="Ebenezer">Ebenezer</option>
                            <option value="Oasis">Oasis</option>
                            <option value="MotherLand">MotherLand</option>
                        </optgroup>

                        <optgroup label="Campus Residences">
                            <option value="Hostel 1">Hostel 1</option>
                            <option value="Hostel 4">Hostel 4</option>
                            <option value="Hostel 6">Hostel 6</option>
                            <option value="Hostel 7">Hostel 7</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <select class="form-control" id="account-type">
                        <option selected="">Select Accout Type</option>
                        <option value="personal">Personal</option>
                        <option value="business">Business</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="business-description-div"><!-- business description -->
                <textarea class="form-control floating-label" rows="2" id="business-description"  placeholder="Describe your Business"></textarea>
                <!--<input type="file" id="business_image" title="Add an image" class="btn btn-danger">
                <span class="help-block">* Upload business Poster, no video or any other file format allowed.</span>-->
            </div>
            <span class="btn btn-info" id="edit-profile-now">Save</span>
            
            
        </div>
    </div><!-- edit profile -->
    
    <div class="col-sm-27">
        <div style="padding:10;">
            
            <div id="alert-box">
                <script>
                    //makeAlert("Please edit your profile details to start posting items!!");
                </script>
            </div>
            
            <div class="bg-white">
                <h3 class="text text-primary">My items</h3>
            </div>
                
            <div id="mproductDetail" class="hidden-xs"><!-- item data here--></div>
            <div id="mproductDetailxs" class="visible-xs"><!-- item data here--></div>
        </div>
    </div>
    
</div>


<div id="cover"><h1>Loading Awesome</h1></div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
    
    $(function() {
        $('#new-username').on('keypress', function(e) {
            if (e.which == 32)
                return false;
        });
    });
    
    $('.alert .close').on('click', function(e) {
        $(this).parent().fadeOut("slow");
    });
</script>

</body>
</html>