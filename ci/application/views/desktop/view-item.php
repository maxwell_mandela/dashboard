

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
/**
* ItemDetaildata factory
*/
var query = new Parse.Query("Product");
query.descending("createdAt");
query.equalTo("objectId", "<?php print $objectId;?>");
query.include("user");
query.find({
  success: function(results) {
    var seller_id = " ";
    $("#fetching-item").fadeOut("slow");
      
    for (var i = 0; i < results.length; i++) {
      var object = results[i];
      var pic = object.get("product_image");
      var pics = object.get("item_images");
      var the_img = pic.url();
      var user = object.get("user");
      var seller = user.get("username");
      var seller_id = user.id;
        
      if(object.get('product_category') != 'Accommodation'){
        $('#house_data').hide();
          
        $('#productDetail').append('<img src='+the_img+' class="img-detail">' 
         +'<div class="content item-content"><h4 class="list-group-item-heading">'
         +object.get('product_title')+'   '+object.createdAt.toLocaleString()+' '
         +'<span class="lead text text-info">, Ksh. '
         +object.get('product_price')+'</span></h4>'
         +'<p class="list-group-item-text">'+object.get('product_description')+'</p>'
         +'<p class="list-group-item-text">'
         +'<span class="mdi-maps-place text text-danger"</span>'
         +object.get("product_seller_location")
         +'</p>'
         +'</div>');

      $("#seller").html("<span class='text text-info'><span class='mdi-action-account-circle'></span>"
        +"<a href='http://swap254.com/user/profiles/"+seller+"'>"+seller+"</a></span>");
        
      $("#seller_info").append('<p><a href="tel:'
           +user.get('user_phone_number')+'"><span class="mdi-communication-call"></span>'
           +user.get('user_phone_number')+'</a></p>'
           +'<p><span class="mdi-action-schedule text text-info"></span>Member Since'
           + '  ' +user.createdAt.toLocaleString()+'</p>');
      }else{
         $('#productDetail').append('<img src='+the_img+' class="img-detail">' 
             +'<div class="content item-content"><h4 class="list-group-item-heading">'
             +object.get('product_title')+'   '+object.createdAt.toLocaleString()+' '
             +'<span class="lead text text-info">, Ksh. '
             +object.get('product_price')+'</span></h4>'
             +'<p class="list-group-item-text">'+object.get('product_description')+'</p>'
             +'<p class="list-group-item-text">'
             +'<span class="mdi-maps-place text text-danger"</span>'
             +object.get("product_seller_location")
             +'</p>'
         +'</div>');

         $('#house_data').append('<div>'
                 +'<p class="list-group-item-text">Type: '+object.get('house_type')+ object.get("item_images")+'</p>'
                 +'<p class="list-group-item-text">Design: '+object.get('house_design')+'</p>'
                 +'<p class="list-group-item-text">Owner: '+object.get('owner_name')+'</p>'
                 +'<p class="list-group-item-text">Units Available: '
                 +object.get('number_of_rooms_available')+'</p>'
             +'</div>');

          $("#seller").html("<span class='text text-info'><span class='mdi-action-account-circle'></span>"
            +"<a href='http://swap254.com/user/profiles/"+seller+"'>"+seller+"</a></span>");

          $("#seller_info").append('<p><a href="tel:'
               +user.get('user_phone_number')+'"><span class="mdi-communication-call"></span>'
               +user.get('user_phone_number')+'</a></p>'
               +'<p><span class="mdi-action-schedule text text-info"></span>Member Since'
               + '  ' +user.createdAt.toLocaleString()+'</p>');
      }
        
      
    }
  },
  error: function(error) {
    setTimeout(function(){
        toastr.error("Sorry, there's been a connection error, please check your internet connection");
    }, 1500);
    $("#fetching-item").fadeOut("slow");
  }
});
</script>

<div class="row">
    <div class="col-sm-25">
        <div style="padding:10;" class="thumbnail">
            <div id="productDetail">
                <!-- item data here-->
                <h2 id="fetching-item" class="text text-info text-center">Getting item details...</h2>
            </div>
            
            <div id="house_data"></div>
            <div id="share"></div>
        </div>
        
        <!--<div id="edit-prompt"></div>-->
    </div>
    
    <div class="col-sm-11">
        <div class="bg-white">
            <h2>Posted by:</h2>
            <h4 class="text text-info"><span id="seller"></span></h4>
            
            <!-- Aside Menu -->
            <div id="seller_info"></div>
        </div><!-- categories menu -->
    </div>
</div>

<div id="cover"><h1>Fetching item</h1></div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
    
    
    
    $("#share").jsSocials({
        shares: ["facebook", "googleplus", "whatsapp"],
        text: ""
    });
</script>
        
</body>
</html>