<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php add_js(
    array('search.js',
          'app/add-product.js')
    );?>

<!-- add new item -->
<div  id="page">
<div class="col-sm-30 col-sm-offset-3 bg-white">
    <h2 class="text text-info">Swap an item</h2>
    <p class="text text-danger" id="check_login"></p>
    
    <form class="form-horizontal">
          <fieldset>
              
          <div class="form-group">
                <label for="product_category" class="col-sm-6 control-label">Change Item Category</label>

                <div class="col-sm-30">
                    <select class="form-control" id="product_category" required>
                        <option selected="selected">Select Category</option>
                        <option value="Phones">Phones/Tablets</option>
                        <option value="Electronics">Electronics/Video</option>
                        <option value="Computers">Computers/Accessories</option>
                        <option value="Fashion">Fashion/Clothes</option>
                        <option value="Electronics">Electronics</option>
                        <option value="Furniture">Furniture</option>
                        <option value="Services">Services</option>
                        <option value="Food">Food/Eat out</option>
                        <option value="Accommodation">Accomodation/Hostels</option>
                    </select>
                </div>
            </div>
              
            <div class="one_image">
                <div class="form-group">
                  <label for="product_title" class="col-sm-6 control-label">Item Name</label>
                  <div class="col-sm-15">
                      <input type="text" maxlength="50" id="product_title" placeholder="Item Name" class="form-control" required>
                  </div>
                    
                  <div class="col-sm-15">
                      <input type="number" maxlength="10" id="item_price" placeholder="Item Price" class="form-control" required>
                      <span class="help-block">Price in number e.g. 12000</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="product_title" class="col-sm-6 control-label">Item Description</label>
                    
                    <div class="col-md-30">
                        <textarea class="form-control floating-label" rows="2" id="product_description"  placeholder="Short Description" maxlength="200" required></textarea>
                        <span class="help-block">A small description(maximum words: 200)</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="product_image" class="col-sm-6 control-label">Item Image</label>
                    <div class="col-sm-30">
                        <input type="file" id="product_image" title="Add an image" class="btn btn-danger" required>
                        <span class="help-block">You have to include an Image and it must be not more than 200kb in size, no video or any other file format allowed.</span>
                        <div id="dvPreview"></div>
                    </div>
                </div>
                
            </div>

            <div class="house_images">
                <div class="form-group">
                  <label for="house_title" class="col-sm-6 control-label">House name</label>

                  <div class="col-sm-15">
                    <input class="form-control" id="house_title" placeholder="Apartment/Hostel/House name" type="text" required>
                  </div>

                  <div class="col-sm-15">
                    <input class="form-control" id="owner_name" placeholder="Enter the LandLord/Landlady name" type="text" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="house_size" class="col-sm-6 control-label">House data</label>

                  <div class="col-sm-10">
                    <input class="form-control" id="house_size" placeholder="Apartment/Hostel/House Size(sq ft)" type="" required>
                    <span class="help-block">Please state the house size in sq ft.</span>
                  </div>

                  <div class="col-sm-10">
                    <input class="form-control" id="number_of_rooms_available" placeholder="Number of Units Available" type="number" required>
                    <span class="help-block">How many Units do you have available?</span>
                  </div>

                  <div class="col-sm-10">
                    <select class="form-control" id="house_type">
                        <option selected="selected" disabled>Select House Type</option>
                        <option value="Student Hostel">Student Hostel</option>
                        <option value="Family Apartment/House">Family Apartment/House</option>
                        <option value="Office Building">Office Building</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="house_design" class="col-sm-6 control-label">House Design</label>

                  <div class="col-sm-10">
                    <select class="form-control" id="house_design">
                        <option selected="selected" disabled>Select House Design</option>
                        <option value="Hostel">Self Contained</option>
                        <option value="Hostel">Not Self contained</option>
                    </select>
                  </div>
                    
                  <div class="col-sm-10">
                    <input class="form-control" id="house_price" placeholder="Monthly Rent" type="text" required>
                  </div>
                 
                  <div class="col-sm-10">
                     <input class="form-control" id="payment_means" placeholder="Payment Means" type="text" required>
                     <span class="help-block">*You can add more than one payment means</span>
                  </div>
                </div>
                
                <div class="form-group">
                    <label for="house_features" class="col-sm-6 control-label">House Features</label>
                    <div class="col-sm-30">
                      <textarea class="form-control" rows="1" id="house_features" maxlength="500"></textarea>
                      <span class="help-block">List the house features each in a new line.</span>
                    </div>
                </div>

                <div class="form-group">
                  <label for="textArea" class="col-sm-6 control-label">House Desription</label>

                  <div class="col-sm-30">
                    <textarea class="form-control" rows="3" id="house_description" maxlength="500"></textarea>
                    <span class="help-block">Describe the hostel, provide as much details as you can.</span>
                  </div>
                </div>

                <div class="form-group">
                    <label for=" " class="col-sm-6 control-label">Note</label>
                    <div class="col-sm-30">
                        <span class="help-block">*Please add an image of the house</span>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label for=" " class="col-sm-6 control-label">Image</label>
                    <div class="col-sm-30">
                        <div class="row">
                            <div class="col-sm-7">
                                <input type="file" id="front_image" title="Add image" class="btn btn-danger" required>
                                <div id="front_image_preview"></div>
                            </div>

                            <!--<div class="col-sm-8">
                                <input type="file" id="rear_image" title="rear image" class="btn btn-danger" required>
                                <div id="rear_image_preview"></div>
                            </div>

                            <div class="col-sm-10">
                                <input type="file" id="interior_image" title="Interior image" class="btn btn-danger" required>
                                <div id="interior_image_preview"></div>
                            </div>

                            <div class="col-sm-10">
                                <input type="file" id="washroom_image" title="washroom mages" class="btn btn-danger" required>
                                <div id="washroom_image_preview"></div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div><!-- houses -->
            
            <div class="form-group">
                <label for="sell" class="col-sm-6 control-label"></label>

                  <div class="col-sm-30">
                    <button id="sell" class="btn btn-info">Post</button>
                  </div>
            </div>
    </fieldset>
</form>
    
</div>
<div class="col-sm-offset-5"></div>

<div id="cover">
    <div class="row">
        <h1>Swap254</h1>
    </div>
    
    <h3>One Moment please...</h3>
    <p>Can't get through this? <a href="<?php echo base_url()?>" class="text text-white">Click here</a></p>
    <p>
        <a href="<?php echo base_url()?>">
            <span class="mdi-navigation-refresh text text-white" style="font-size:3em;"></span></a></p>
</div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
        $("form").validate();
    });
</script>
