<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- add new item --
<div class="col-sm-7 bg-white">
    <h4 class="text text-info">Find from a comrade</h4>
    
    <span class="">
        Can't find what you are looking for? Post it here and sit back, you will be notified when its available.
    </span>
    
    <div class="row">
      <div class="form-group col-xs-18 col-sm-18 col-md-18">
          <input type="text" maxlength="50" id="demand_item_title" placeholder="Item Name" class="form-control">
      </div>
      
      <div class="form-group col-xs-18 col-sm-18 col-md-18">
            <select class="form-control" id="product_category">
                <option selected="selected">Select Category</option>
                <option value="Phones">Phones/Tablets</option>
                <option value="Electronics">Electronics/Video</option>
                <option value="Computers">Computers/Accessories</option>
                <option value="Fashion">Fashion/Clothes</option>
                <option value="Electronics">Electronics</option>
                <option value="Furniture">Furniture</option>
                <option value="Services">Services</option>
                <option value="Food">Food/Eat out</option>
                <option value="Accommodation">Accomodation/Hostels</option>
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-md-18">
          <input type="number" maxlength="10" id="item_price" placeholder="Item Price" class="form-control">
          <span class="help-block">
                Price in number e.g. 12000</span>
        </div>
      
        <div class="form-group col-md-18">
            <textarea class="form-control floating-label" rows="2" id="product_description"  placeholder="Short Description" maxlength="200"></textarea>
            <span class="help-block">
                A small description(maximum words: 200)</span>
        </div>
    </div>
    
    <div class="form-group">
        <input type="file" id="product_image" title="Add an image" class="btn btn-danger">
        <span class="help-block">You have to include an Image and it must be not more than 200kb in size, no video or any other file format allowed.</span>
    </div>
    <span id="sell" class="btn btn-info">Post</span>
</div>
</div><!-- end swap main-->