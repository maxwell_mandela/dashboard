<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php add_js(array(
    'app/add-business.js',
    'app/get-demands.js'
));?>

<!-- add new item -->
<div  id="page">
<div id=" " class="col-sm-30 col-sm-offset-3 bg-white">
    <h2 class="text text-info">Add Business</h2>
    <p class="text text-danger" id="check_login"></p>
    
    <form class="form-horizontal">
          <fieldset>
              
          <div class="form-group">
                <label for="business_category" class="col-sm-6 control-label">Business Category:</label>

                <div class="col-sm-8">
                    <select class="form-control" id="business_category" required>
                        <option selected="selected" disabled>Select Category</option>
                        <option value="Service">Service</option>
                        <option value="Shop">Shop/Retail</option>
                        <option value="Finacial">Finacial</option>
                        <option value="Real Estate">Real Estate</option>
                        <option value="Transport">Transport</option>
                    </select>
                </div>
              
                <label for="business_name" class="col-sm-6 control-label">Business Name:</label>
                <div class="col-sm-16">
                  <input type="text" maxlength="50" 
                         id="business_name" 
                         placeholder="Business Name" class="form-control" required>
                </div>
            </div>
              
            <div class="form-group">
                <label for="business_description" class="col-sm-6 control-label">Business Description:</label>

                <div class="col-sm-30">
                    <textarea class="form-control floating-label" rows="2" id="business_description"  placeholder="Enter Description" maxlength="200" required></textarea>
                    <span class="help-block">A small description(maximum words: 200)</span>
                </div>
            </div>
              
            <div class="form-group">
              <label for="business_contacts" class="col-sm-6 control-label">Business Contacts:</label>
                <div class="col-sm-15">
                    <textarea class="form-control floating-label" rows="2" id="business_contacts"  placeholder="Enter Business Contacts" maxlength="200" required></textarea>
                    <span class="help-block">A list of contacts(maximum words: 200)</span>
                </div>
                
                <div class="col-sm-15">
                    <select class="form-control" id="business_location" required>
                        <optgroup label="Diaspora">
                            <option selected="selected" disabled>Select Business Location</option>
                            <option value="Tea Farm">Tea Farm</option>
                            <option value="Chepnyogaa">Chepnyogaa</option>
                            <option value="Kapmaso">Kapmaso</option>
                            <option value="Judea">Judea</option>
                            <option value="Kapcheluch">Kapcheluch</option>
                            <option value="Kabianga Market">Kabianga Market</option>
                            <option value="KMS">KMS</option>
                            <option value="MaryLand">Mary Land</option>
                            <option value="Keter">Keter</option>
                            <option value="Destiny">Destiny</option>
                            <option value="Sawa Hostels">Sawa Hostels</option>
                            <option value="Landmark">Landmark</option>
                            <option value="Ebenezer">Ebenezer</option>
                            <option value="Oasis">Oasis</option>
                            <option value="MotherLand">MotherLand</option>
                        </optgroup>

                        <optgroup label="Campus Residences">
                            <option value="Hostel 1">Hostel 1</option>
                            <option value="Hostel 4">Hostel 4</option>
                            <option value="Hostel 6">Hostel 6</option>
                            <option value="Hostel 7">Hostel 7</option>
                        </optgroup>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="business_poster" class="col-sm-6 control-label">Business Poster</label>
                <div class="col-sm-30">
                    <input type="file" id="business_poster" title="Add an image" class="btn btn-danger" required>
                    <span class="help-block">You have to include an Image and it must be not more than 200kb in size, no video or any other file format allowed.</span>
                    <div id="dvPreview"></div>
                </div>
            </div>
                
            
            <div class="form-group">
                <label for="add_business_now" class="col-sm-6 control-label"></label>

                  <div class="col-sm-30">
                    <button id="add_business_now" class="btn btn-info">Add</button>
                  </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="col-sm-offset-5"></div>

<div id="cover">
    <div class="row">
        <h1>Swap254</h1>
    </div>
    
    <h3>One Moment please...</h3>
</div>
    
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
</script>