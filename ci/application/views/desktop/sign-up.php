<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body style="background-color:#056;">
    <div class="col-sm-10"></div>
    
    <div class="col-sm-16 jumbotron" style="margin-top:5%;">
        
        <div class="row">
            <div class="col-sm-18">
                <h2 class="text text-info text-center">Sign Up</h2>
                <p class="text-center" style="font-size:1.3em;"><span class="sup">*</span>Please sign up with your correct email address as your account will be deactivated should this be violated. Thank you</p>
            </div>
            
            <div class="col-sm-18">
                <img class="img-responsive" src="<?php echo base_url();?>ci/style/img/logo.jpg">
            </div>
        </div>
        
            <?php $data = array(
                'the_name' => array(
                    'name' => 'username',
                    'maxlength' => 20,
                    'id'   => 'username',
                    'type' => 'text',
                    'placeholder' => 'Username e.g Smith',
                    'class'       => 'form-control'
                ),
                'email' => array(
                    'name'        => 'email',
                    'id'          => 'email',
                    'type'        => 'email',
                    'placeholder' => 'Email',
                    'class'       => 'form-control'
                ),
                'password' => array(
                    'name'        => 'password',
                    'id'          => 'password',
                    'type'        => 'password',
                    'placeholder' => 'Password',
                    'class'       => 'form-control'
                )
            );

            foreach($data as $input_field){
                echo form_input($input_field);
            }
        ?>
        <select class="form-control" id="residence">
            <optgroup label="Diaspora">
                <option selected="selected">Select your residence</option>
                <option value="Tea Farm">Tea Farm</option>
                <option value="Chepnyogaa">Chepnyogaa</option>
                <option value="Kapmaso">Kapmaso</option>
                <option value="Judea">Judea</option>
                <option value="Kapcheluch">Kapcheluch</option>
                <option value="Kabianga Market">Kabianga Market</option>
                <option value="KMS">KMS</option>
                <option value="MaryLand">Mary Land</option>
                <option value="Keter">Keter</option>
                <option value="Destiny">Destiny</option>
                <option value="Sawa Hostels">Sawa Hostels</option>
                <option value="Landmark">Landmark</option>
                <option value="Ebenezer">Ebenezer</option>
                <option value="Oasis">Oasis</option>
                <option value="MotherLand">MotherLand</option>
            </optgroup>
            
            <optgroup label="Campus Residences">
                <option value="Hostel 1">Hostel 1</option>
                <option value="Hostel 4">Hostel 4</option>
                <option value="Hostel 6">Hostel 6</option>
                <option value="Hostel 7">Hostel 7</option>
            </optgroup>
        </select>
            
        <br>
        <p class="help-block">
            By clicking <strong>Sign Up</strong> you agree to Swap254 
            <a href="<?php echo base_url("about/terms_conditions");?>">Terms and Conditions</a>
        </p>
        <span id="sign-up" class="btn btn-info">Sign Up</span>
         Or
        <a class="" href="login">Login</a> (if you have an account)
        <br>
        <span id="response" class="text text-center text-primary"></span>
        
    </div>
    
    <div class="col-sm-10"></div>
    
<div id="cover"><h1>Loading Page</h1></div>
<script>
    $(window).on('load', function() {
        $("#cover").fadeOut("slow");
    });
    
    $(function() {
        $('#username').on('keypress', function(e) {
            if (e.which == 32)
                return false;
        });
    });
    
    /*facebook login*
    $(document).ready(function(){
        $("#facebook-login").click(function(){
            $("#facebook-login").html("loading...");
            
            //Initialize the Parse object first.
            //Parse.initialize("GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3", "GgtZidl1lSXbDBWQKhif8glrjdJVTv8Z1I7ZwgED");

            window.fbAsyncInit = function() {
                Parse.FacebookUtils.init({
                    appId      : '1516476868672647', 
                    status     : true, 
                    cookie     : true, 
                    xfbml      : true
                });
                
                Parse.FacebookUtils.logIn(null, {
                    success: function(user) {
                        if (!user.existed()) {
                            alert("User signed up and logged in through Facebook!");
                        } else {
                            alert("User logged in through Facebook!");
                        }
                    },
                    error: function(user, error) {
                        alert("User cancelled the Facebook login or did not fully authorize.");
                    }
                });
            };

            (function(d){
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
    
        });
    });*/
</script>
    
</body>
</html>