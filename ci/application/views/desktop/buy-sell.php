<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php add_js(
    array('app/get-products.js',
           'app/add-product.js')
    );?>


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1516476868672647',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div class="row" id="swap-main">
    <div class="col-sm-9">
        <div class="bg-white">
            
            <div class="row">
                <div class="col-sm-36 hidden-xs">
                    <img class="img-responsive" src="<?php echo base_url();?>ci/style/img/logo.jpg">
                </div>
                
                <div class="col-xs-12 col-xs-offset-12 visible-xs">
                    <img class="img-responsive" src="<?php echo base_url();?>ci/style/img/logo.jpg">
                </div>
            </div>
            
            <h2 class="hidden-xs text text-info">Categories</h2>
            <div class="hidden-xs">
                <p><a href="swap/categories/Phones">Phones/Tablets</a></p>
                <p><a href="swap/categories/Electronics">Electronics/Video</a></p>
                <p><a href="swap/categories/Computers">Computers/Accessories</a></p>
                <p><a href="swap/categories/Accommodation">Accomodation/Hostels</a></p>
                <p><a href="swap/categories/Services">Services/Jobs</a></p>
                <p><a href="swap/categories/Food">Eat out/Food</a></p>
                <p><a href="swap/categories/Furniture">Furniture</a></p>
                <p><a href="swap/categories/Fashion">Fashion/Clothes</a></p>
                <p><a href="swap/categories/Computers">Computers/Accessories</a></p>
            </div>
            
            <h3 class="visible-xs text text-info">Change Swapping Category</h3>
            <div class="dropdown visible-xs">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle btn btn-info btn-block text-left">Categories <b class="caret"></b></a>
                <ul class="dropdown-menu" style="width:100%;position:absolute; z-index:1000;">
                    <li><a href="swap/categories/Phones">Phones/Tablets</a></li>
                    <li><a href="swap/categories/Electronics">Electronics</a></li>
                    <li><a href="swap/categories/Accommodation">Accomodation/Hostels</a></li>
                    <li><a href="swap/categories/Services">Service/JObss</a></li>
                    <li><a href="swap/categories/Food">Eat Out/Food</a></li>
                    <li><a href="swap/categories/Furniture">Furniture</a></li>
                    <li><a href="swap/categories/Fashion">Fashion/Clothes</a></li>
                    <li><a href="swap/categories/Computers">Computers</a></li>
                </ul>
            </div>
            
        </div><!-- categories -->

        <div class="bg-white hidden-xs">
            <h3 class="text text-info">Underway Developments</h3>
            
            <div class="hidden-xs">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <span class="text text-center">Business/Shop Dashboard set up</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Editable user content</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Business/shop products dashboard</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Stores for swap254 own products!</span>
                    </div>
                  </div>
                </div>
            </div>
            
            <div class="visible-xs">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1. What is HTML?</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages. <a href="http://www.tutorialrepublic.com/html-tutorial/" target="_blank">Learn more.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2. What is Bootstrap?</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Bootstrap is a powerful front-end framework for faster and easier web development. It is a collection of CSS and HTML conventions. <a href="http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div><!-- popular places -->
        
        <!-- add button -->
        <button  class="btn btn-info" style="bottom:0; position:fixed;z-index:1000;" id="show-add-div">
            Sell Now!</button>
        
    </div>

    <div class="col-sm-20">
        <div ng-controller="MainItemsController" class="bg-white" style="padding:10;">
            <div class="row hidden-xs">
                <div class="col-sm-18">
                    <h3 class="text text-info">New Items</h3> 
                </div>
                
                <div class="col-sm-18">
                    <h3 class="text text-info" id="saved_location" ng-bind="saved_location"></h3> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-21 col-xs-19">
                  <label for="search">Search:</label>
                  <input ng-model="q" class="form-control" placeholder="Search Anything">
                </div>
                
                <div class="col-sm-15 col-xs-17">
                      <label for="search">Change Location</label>
                      <select class='form-control' 
                        data-ng-model='selectedOption' 
                        data-ng-options='item as item.name for item in collections' 
                        ng-change="changedValue()"></select>
                </div>
                
                <!--<div class="col-sm-9 hidden-xs">
                    <div class="btn-group">
                        <a href="#" id="list" class="btn btn-info btn-sm">
                            <span class="mdi-action-view-list"></span>
                        </a>
                        <a href="#" id="grid" class="btn btn-info btn-sm">
                            <span class="mdi-action-view-module"></span>
                        </a>
                    </div>
                </div>-->
              </div>
            <br>
            
            <div id="show_no_posts">
                <p>Sorry, we didn't find posts form this location, please try browsing different location</p>
                <p>If you'd like to add an item, click the 'sell now' button at the botom of the page</p>
            </div>
            
            <div id="products" class="hidden-xs row list-group">
                <div class="item cursor-hand col-xs-36" dir-paginate="item in items | filter:q | itemsPerPage: pageSize" current-page="currentPage">
                    <div class="thumbnail" ng-click="getItemDetails($index)">
                        <img class="group list-group-image" src="{{item.product_image.url}}" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">{{item.product_title}}
                                <span class="lead text text-info">Ksh. {{item.product_price}}</span>
                                <span class="item-date text text-right">{{item.createdAt | date:'yyyy/MM/dd'}}</span></h4>
                            <p class="group inner list-group-item-text">{{item.product_description | words:15}}</p>
                            <div class="group row">
                                <div class="col-sm-20 col-xs-20">
                                    <span>Category: <a href="#">{{item.product_category}}</a></span>
                                        <p class="text text-right">
                                            <a href="tel:{{item.user.user_phone_number}}">
                                                <span class="mdi-communication-call text text-success"></span>  
                                                {{item.user.user_phone_number | splitcharacters:4}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="products-xs" class="visible-xs">
                <div class="row grid-group">
                    <div class="item col-xs-36" dir-paginate="item in items | filter:q | itemsPerPage: pageSize" current-page="currentPage">
                        <div class="thumbnail" ng-click="getItemDetails($index)">
                            <img class="group grid-group-image" src="{{item.product_image.url}}" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">{{item.product_title}}
                                    <span class="item-date">{{item.createdAt | date:'yyyy/MM/dd'}}</span></h4>
                                <p class="group inner list-group-item-text">{{item.product_description | words:15}}</p>
                                <div class="">
                                    <p class="lead text text-info">Ksh. {{item.product_price}}</p>
                                    <p>Category: <a href="#">{{item.product_category}}</a>,
                                        <span class="text text-right">
                                            <span class="mdi-communication-call text-text-success"></span>  
                                            {{item.user.user_phone_number}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
                  <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)"  template-url="<?php echo base_url();?>dirPagination.tpl.html">
                  </dir-pagination-controls>
              </div>
            </div>
        </div>
    </div>
    
    <!-- add new demand item -->
    <div class="col-sm-7">
        <div class="bg-white" style="margin-bottom:50px;">
            <h4 class="text text-info">Find from a comrade</h4>
    
            <span class="help-block">
                Can't find what you are looking for? Post it here and sit back, you will be notified when its available.
            </span>
            <br>
            
          <div class="form-group">
              <input type="text" maxlength="50" id="demand_item_title" placeholder="Enter title" class="form-control">
          </div>

          <div class="form-group">
                <select class="form-control" id="demand_item_category">
                    <option selected="selected">Select Category</option>
                    <option value="Phones">Phones/Tablets</option>
                    <option value="Electronics">Electronics/Video</option>
                    <option value="Computers">Computers/Accessories</option>
                    <option value="Fashion">Fashion/Clothes</option>
                    <option value="Electronics">Electronics</option>
                    <option value="Furniture">Furniture</option>
                    <option value="Services">Services</option>
                    <option value="Food">Food/Eat out</option>
                    <option value="Accommodation">Accomodation/Hostels</option>
                </select>
            </div>

            <div class="form-group">
              <input type="number" maxlength="10" id="demand_item_price" placeholder="Enter Price" class="form-control">
              <span class="help-block">
                    The price you would buy this item at e.g. 12000</span>
            </div>

            <div class="form-group">
                <textarea class="form-control floating-label" rows="2" id="demand_item_description"  placeholder="Short Description" maxlength="200"></textarea>
                <span class="help-block">
                    (maximum words: 100)</span>
            </div>
            <span id="post-demand" class="btn btn-info">Post item</span>
        </div>
    </div>
</div><!-- end swap main-->
</div>

<script>
    $('#products .item').addClass('list-group-item');
    $(document).ready(function() {
        $('#products .item').addClass('list-group-item');
        $('#list').click(function(event){
            event.preventDefault();$('#products .item').addClass('list-group-item');});

        $('#grid').click(function(event){
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');});
    });
</script>