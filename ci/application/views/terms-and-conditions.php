
<div class="container">
    <div class="col-sm-20 col-sm-offset-8 bg-white">
        <?php echo anchor("swap", "Back Home");?>
        
        <h2 class="text text-info">Swap254 Terms and Conditions</h2>

        <h5 class="text"><strong>Please read carefully these Terms and Conditions</strong></h5>

        <p>Last modified on Novermber 14<sup>th</sup>, 2015.</p>
        <p>This Agreement documents the legally binding terms and conditions attached to the use of <a href="<?php echo base_url();?>">swap254.com</a>.</p>
        
        <p>By using or accessing the Site in any way, viewing or browsing the Site, or adding your own content to the Site, you are agreeing to be bound by these Terms of Service.</p>
        
        <h3>Intellectual Property</h3>
        
        <p>The Site and all of its original content are the sole property of Swap254 and are, as such, fully protected by the appropriate copyright and other intellectual property rights laws.</p>
            
        <h3>Termination</h3>
        
        <p>Swap254 reserves the right to terminate your access to the Site, without any advance notice should you 
            violate any of the following:
            <ul>
                <li>Do not post anything or any image that can lead to social conflicts within the University Students</li>
                <li>Keep all you posts in either English of Kiswahili, other langiuages arew not allowed as they are not well spoken by everyone that use Swap254 and we do not offer translation services yet</li>
                <li>Keep you account details accurate such as phone number and email address. We do not use these details in harmful way and we do not share them with a third party whasoever. All your details are protected</li>
                <li>Posts containing insults, hate speeches will immediately lead to account suspension or removal by
                the team@swap254.com</li>
                <li>Swap254 requires all users to practice good business ethics therefore posts intended to ruin other businesses will not be tolerated</li>
        </ul>
            
        <h3>Links to Other Websites</h3>

        <p>Our Site does contain a number of links to other websites and online resources that are not owned or controlled by Swap254.</p>
        
        <p>Swap254 has no control over, and therefore cannot assume responsibility for, the content or general practices of any of these third party sites and/or services. Therefore, we strongly advise you to read the entire terms and conditions and privacy policy of any site that you visit as a result of following a link that is posted on our site.</p>
        
        <p><strong>Continuing to use Swap254 means you totally agree with our terms of use.</strong></p>
    </div>
</div>