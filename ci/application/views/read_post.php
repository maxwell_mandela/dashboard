<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body class="container">
    
    <div class="col-md-26 col-md-offset-5">
        <div class="the-content card">
          <div class="image">
              <img src='{image}' class='img-responsive image'>
          </div>
          <div class="content">
              <span class="title"><h3>{title}</h3></span>
              <div style="font-family: 'Dosis',sans-serif; font-weight:350; color:#363744;">{content}</div>
          </div>
        </div>
    </div>
</body>