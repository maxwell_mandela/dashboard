<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
    <div class="col-md-10"></div>
    <div class="col-md-16 bg-white" style="margin-top:10%;">
        <h2 class="text text-info">Sign In</h2>
        <?php echo form_open("login/finishing");?>
            <?php $data = array(
                'the_name' => array(
                    'name' => 'username',
                    'type' => 'text',
                    'placeholder' => 'Username',
                    'class' => 'form-control'
                ),
                'password' => array(
                    'name'        => 'password',
                    'type'        => 'password',
                    'placeholder' => 'Password',
                    'class'       => 'form-control'
                )
            );

            foreach($data as $input_field){
                echo form_input($input_field);
            }
        ?>
        <button type="submit" class="btn btn-info submit">Log In</button>
        </form>
    
        <a class="text text-info cursor-hand" style="float:right;" href="passwordreset">Forgot Password??</a>
        <?php echo br();?>
        <p class="text text-info">Need an account?</p>
        <a class="text text-primary cursor-hand" href="register">Create an account</a>
        <br>
        <span id="response" class="text text-center text-primary"></span>
    </div>
    <div class="col-md-10"></div>
</body>
</html>