<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-sm-9">
        <div class="bg-white">
            <h3 class="hidden-xs text text-info">Categories</h3>
            <div class="hidden-xs">
                <p><a href="<?php echo base_url("swap/categories/Phones");?>">Phones/Tablets</a></p>
                <p><a href="<?php echo base_url("swap/categories/Electronics");?>">Electronics</a></p>
                <p><a href="<?php echo base_url("swap/categories/Accommodation");?>">Accomodation/Hostels</a></p>
                <p><a href="<?php echo base_url("swap/categories/Services");?>">Services/Jobs</a></p>
                <p><a href="<?php echo base_url("swap/categories/Food");?>">Eat out/Food</a></p>
                <p><a href="<?php echo base_url("swap/categories/Furniture");?>">Furniture</a></p>
                <p><a href="<?php echo base_url("swap/categories/Fashion");?>">Fashion/Clothes</a></p>
                <p><a href="<?php echo base_url("swap/categories/Computers");?>">Computers</a></p>
            </div>
            
            <h4 class="visible-xs text text-info">Change Swapping Category</h4>
            <div class="dropdowvn visible-xs">
                <select name="" class="form-control">
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Phones");?>">
                        Phones/Tablets</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Electronics");?>">Electronics</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Accommodation");?>">
                            Accomodation/Hostels</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Services");?>">
                            Services</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Food");?>">
                            Eat Out/Food</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Furniture");?>">
                            Furniture</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Fashion");?>">
                            Fashion/Clothes</a></option>
                    
                    <option value="">
                        <a href="<?php echo base_url("swap/categories/Computers");?>">
                            Computers</a></option>
                </select>
            </div>
            
        </div><!-- categories -->

        <div class="bg-white hidden-xs">
            <h3 class="text text-info">Underway Developments</h3>
            
            <div class="hidden-xs">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <span class="text text-center">Business/Shop Dashboard set up</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Editable user content</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Business/shop products dashboard</span>
                    </div>

                    <div class="item">
                      <span class="text text-center">Stores for swap254 own products!</span>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-sm-27 col-xs-36">
        <div class="bg-white">
            <?php getProductsFromCategory($category_name); ?>
        </div>
    </div>
    
</div>