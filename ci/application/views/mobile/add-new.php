<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- add new item -->
<div id="add-new" class="col-sm-26 col-sm-offset-5 bg-white">
    <h2 class="text text-info">Swap an item</h2>
    <?php echo form_open_multipart("swap/finish_posting");?>
    <div class="row">
      <div class="form-group col-xs-18 col-sm-18 col-md-18">
          <input type="text" maxlength="50" required name="product_title" id="product_title" placeholder="Item Name" class="form-control">
      </div>
      
      <div class="form-group col-xs-18 col-sm-18 col-md-18">
            <select class="form-control" required name="product_category" id="product_category">
                <option selected="selected">Select Category</option>
                <option value="Phones">Phones/Tablets</option>
                <option value="Electronics">Electronics/Video</option>
                <option value="Computers">Computers/Accessories</option>
                <option value="Fashion">Fashion/Clothes</option>
                <option value="Electronics">Electronics</option>
                <option value="Furniture">Furniture</option>
                <option value="Services">Services</option>
                <option value="Food">Food/Eat out</option>
                <option value="Accommodation">Accomodation/Hostels</option>
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-md-18">
          <input type="number" required
                 maxlength="10" 
                 name="product_price" 
                 id="product_price" 
                 placeholder="Enter Price" 
                 class="form-control">
            
          <span class="help-block">
                Price in number e.g. 12000</span>
        </div>
      
        <div class="form-group col-md-18">
            <textarea class="form-control floating-label" rows="2" required
                      name="product_description" id="product_description"  
                      placeholder="Short Description" maxlength="200"></textarea>
            <span class="help-block">
                A small description(maximum words: 200)</span>
        </div>
    </div>
    
    <div class="form-group">
        <input type="file" required name="product_image" id="product_image">
        
        <span class="help-block">You have to include an Image and it must be not more than 200kb in size, no video or any other file format allowed.</span>
    </div>
    <button name="save_item" class="btn btn-info">Post</button>
    </form>
    
</div>
<div class="col-sm-offset-5"></div>