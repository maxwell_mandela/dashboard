<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body style="background-color:#056;">
    <div class="col-sm-10"></div>
    <div class="col-sm-16 jumbotron" style="margin-top:10%;">
        <h2 class="text text-info">Sign Up</h2>
        <?php echo form_open("register/finishing");?>    
            <?php $data = array(
                    'the_name' => array(
                        'name' => 'username',
                        'id' => 'username',
                        'type' => 'text',
                        'placeholder' => 'Username(one word)',
                        'required' => 'required',
                        'class'       => 'form-control'
                    ),
                    'email' => array(
                        'name'        => 'email',
                        'type'        => 'email',
                        'placeholder' => 'email',
                        'required' => 'required',
                        'class'       => 'form-control'
                    ),
                    'password' => array(
                        'name'        => 'password',
                        'type'        => 'password',
                        'required' => 'required',
                        'placeholder' => 'password',
                        'class'       => 'form-control'
                    )
                );

                foreach($data as $input_field){
                    echo form_input($input_field);
                }
            ?>
            <select class="form-control" id="residence" name="residence" required>
                <optgroup label="Diaspora">
                    <option value=" ">Choose residence</option>
                    <option value="Tea Farm">Tea Farm</option>
                    <option value="Chepnyogaa">Chepnyogaa</option>
                    <option value="Kapmaso">Kapmaso</option>
                    <option value="Judea">Judea</option>
                    <option value="Kapcheluch">Kapcheluch</option>
                    <option value="Kabianga Market">Kabianga Market</option>
                    <option value="KMS">KMS</option>
                    <option value="MaryLand">Mary Land</option>
                    <option value="Keter">Keter</option>
                    <option value="Destiny">Destiny</option>
                    <option value="Sawa Hostels">Sawa Hostels</option>
                    <option value="Landmark">Landmark</option>
                    <option value="Ebenezer">Ebenezer</option>
                    <option value="Oasis">Oasis</option>
                    <option value="MotherLand">MotherLand</option>
                </optgroup>

                <optgroup label="Campus Residences">
                    <option value="Hostel 1">Hostel 1</option>
                    <option value="Hostel 4">Hostel 4</option>
                    <option value="Hostel 6">Hostel 6</option>
                    <option value="Hostel 7">Hostel 7</option>
                </optgroup>
            </select>

            <br>
            <button name="register" type="submit" class="btn btn-info">Sign Up</button>
        </form>
         OR 
        <a class="" href="login">Login</a> (If you have an account)
        <br>
        <span id="response" class="text text-center text-primary"></span>
        
    </div>
    <div class="col-sm-10"></div>
    
    <script>
        $(function() {
            $('#username').on('keypress', function(e) {
                if (e.which == 32)
                    return false;
            });
        });
    </script>
</body>
</html>