<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
    <div class="col-md-10"></div>
    <div class="col-md-16 bg-white" style="margin-top:10%;">
        <h2 class="text text-info">Reset your password</h2>
        <?php echo form_open("passwordreset/password_reset_email_sent");?>
            <?php $data = array(
                'reset_email' => array(
                    'name' => 'email',
                    'type' => 'email',
                    'placeholder' => 'Enter your email address',
                    'class' => 'form-control'
                )
            );

            foreach($data as $input_field){
                echo form_input($input_field);
            }
        ?>
        <button type="submit" name="reset_pwd" class="btn btn-info submit">Reset</button>
        </form>
    </div>
    <div class="col-md-10"></div>
</body>
</html>