<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php checkSession();?>
<div class="row">
    <!-- my profile -->
    <div class="col-sm-9">
        <div class="bg-white">
            <h2 class="text text-info"><?php print getUsername();?></h2>
            <!--<h3 id="name"> {{username}}</h3>-->
            <p class="text text-primary">
                <span class="mdi-action-account-circle text text-info"></span>
                <?php 
                    print getFirstName()."  ";
                    print getLastName();
                ?>
            </p>
            <p>
                <span class="mdi-communication-call"></span><?php print getPhoneNumber();?>
            </p>
            <p id="email">
                <span class="mdi-content-mail text text-danger"></span> <?php print getEmail();?></p>
            
            <p><span class="mdi-communication-location-on text text-primary"></span><?php print getResidence();?></p>
            <p class=""><a href="<?php echo  base_url("user/editmyprofile")?>">Edit Profile</a></p>
        </div>
    </div>
    
    
    <div class="col-sm-27">
        <div style="padding:10;">
            
            <div class="bg-white">
                <h3 class="text text-primary">My items</h3>
            </div>
                
            <div>
                <!-- item data here-->
                <?php getUserProductList();?>
            </div>
        </div>
    </div>
</div>