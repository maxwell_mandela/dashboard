<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- load js -->
<?php add_js(array('jquery.js',
                   'bootstrap.min.js',
                   'material.min.js',
                   'ripples.min.js',
                   'toastr.js'))?>

<!-- get benchmark -->
<?php get_benchmark(false);?>

