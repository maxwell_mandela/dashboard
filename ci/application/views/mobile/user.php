<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <?php checkSession();?>
    <!-- my profile -->
    <div class="col-md-9">
        <div class="bg-white">
            <h2 class="text text-info">My Profile</h2>
        </div>
    </div>
    
    <div class="col-md-27">
        <div class="bg-white">
            <h2 class="text text-info">My added items</h2>
            <div id=""><?php getUserProductList(); ?></div>
        </div>
    </div>
</div>
</body>
</html>