<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- edit profile -->
<div class="row">
    <div class="col-sm-26 col-sm-offset-5 col-xs-36">
        <div class="bg-white">
            <h3 class="text text-info" style="cursor: hand;">Edit your Profile</h3>
            <?php echo form_open_multipart("user/editting_profile");?>
            <input type="text" class="form-control" maxlength="30" value="<?php print getUsername();?>" name="username">
            
            <br>
            
            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="text" maxlength="30" name="firstname" placeholder="First Name" class="form-control" value="<?php getFirstName() ?>">
                </div>
                
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="text" maxlength="30" name="lastname" placeholder="Last Name" class="form-control" value="<?php getLastName() ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <input type="number" max="9999999999" class="form-control" placeholder="Your Phone" name="user_phone_number" value="<?php getPhoneNumber() ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <select class="form-control" name="residence">
                        <optgroup label="Diaspora">
                            <option value=" ">Choose residence</option>
                            <option value="Tea Farm">Tea Farm</option>
                            <option value="Chepnyogaa">Chepnyogaa</option>
                            <option value="Kapmaso">Kapmaso</option>
                            <option value="Judea">Judea</option>
                            <option value="Kapcheluch">Kapcheluch</option>
                            <option value="Kabianga Market">Kabianga Market</option>
                            <option value="KMS">KMS</option>
                            <option value="MaryLand">Mary Land</option>
                            <option value="Keter">Keter</option>
                            <option value="Destiny">Destiny</option>
                            <option value="Sawa Hostels">Sawa Hostels</option>
                            <option value="Landmark">Landmark</option>
                            <option value="Ebenezer">Ebenezer</option>
                            <option value="Oasis">Oasis</option>
                            <option value="MotherLand">MotherLand</option>
                        </optgroup>

                        <optgroup label="Campus Residences">
                            <option value="Hostel 1">Hostel 1</option>
                            <option value="Hostel 4">Hostel 4</option>
                            <option value="Hostel 6">Hostel 6</option>
                            <option value="Hostel 7">Hostel 7</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-xs-18 col-sm-18 col-md-18">
                    <select class="form-control" name="acount_type">
                        <option selected="">Select Accout Type</option>
                        <option value="personal">Personal</option>
                        <option value="business">Business</option>
                    </select>
                </div>
            </div>

            <!--<div class="form-group"><!-- business description --
                <textarea class="form-control floating-label" rows="2" name="business-description"  placeholder="Describe your Business"></textarea>
            </div>-->
            <button type="submit" class="btn btn-info" name="edit-profile">Save</button>
            </form>
        </div>
    </div><!-- edit profile -->
</div>