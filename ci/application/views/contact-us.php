<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    body{
        background-color:#056;
        color:#fff;
    }
    .row{
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
    }
    .first-row-content{
        margin:20px;
    }
</style>
    <div class="container">
        
        <div class="rrow first-row-content">
            <div class="col-sm-12 col-xs-36">
                
            </div>
            
            <div class="col-sm-12 col-xs-36">
                <h1>Contact Swap254</h1>
                <small>Swap254 support is available 24hrs a day!</small>
                
                <h2>Info</h2>
                <p>Send all your enquiries to Swap 254 through this email</p>
                <h4 class="text">info@swap254.com</h4>
                
                <hr>
                
                <h2>Careers</h2>
                <h4>Want a job at swap254? We are working on this to list the available positions</h4>
                <p>However you can send your contacts details to the email address below so we can reach you in case of an open position</p>
                <h4 class="text">careers@swap254.com</h4>
            </div>
            
            <div class="col-sm-12 col-xs-36">
            </div>
        </div>
    </div>
</body>
</html>