<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php echo doctype();?>

<html lang="en" ng-app="app">
<head>
	<meta charset="utf-8">
    <title>{title}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php add_css(array('bootstrap.css','roboto.min.css','material.min.css', 'ripples.min.css','loading-bar.css','toastr.css','jssocials.css','jssocials-theme-flat.css','style.css'));?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php
        $meta = array(
            array(
                    'name' => 'description',
                    'content' => 'Swap items within University of Kabianga, trade freely, target all residents from Kabianga to Tea Factory. Swap254 is a Comrade Trade Center for University of Kabianga targeting the diaspora guys who due to long distances to one another will benefit from a central online free market system.'
            ),
            array(
                    'name' => 'keywords',
                    'content' => 'Swap all your stuff for new in swap254, get anything you need from a comrade!!!, find all phones on sale from University of Kabianga and sorroudning, now you can see what your next door neighbour is selling!, find food and view menus before walking in to a restourant!, University of Kabianga diaspora students'
            ),
            array(
                    'name' => 'Content-type',
                    'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
            )
    );

    echo meta($meta);
    ?>
    <meta property="og:image" content="http://swap254.com/ci/style/img/logo.jpg"/>
</head>
    
<body class="container">
    <!-- header -->
    <div style="color:#fff;">
        <div class="navbar navbar-default menu-main">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url()?>"><span class="mdi-action-home"></span></a>
                <a class="navbar-brand" href="<?php echo base_url()?>">Swap254 UoK</a>
                
                <a class="navbar-brand" href="<?php echo base_url("swap/swap_new_item")?>"><span class="mdi-content-add"></span></a>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="<?php echo base_url("about")?>">About</a></li>
                    <li><a href="<?php echo base_url("about/contact_us")?>">Contact Us</a></li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url("user/myprofile")?>">My Account</a></li>
                    <li id="is-logged-in"><a href="<?php echo base_url("login")?>">Sign Up/Login</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <script></script>