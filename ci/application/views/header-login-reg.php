<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php echo doctype();?>

<html lang="en" ng-app="app">
<head>
	<meta charset="utf-8">
    <title>{title} | {sub_title}</title>
    <?php add_css(array('bootstrap.css','roboto.min.css','material.min.css', 'ripples.min.css','toastr.css','style.css'));?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php
        $meta = array(
            array(
                    'name' => 'description',
                    'content' => 'Swap254 | Swap254 is a Comrade Trade Center for University of Kabianga'
            ),
            array(
                    'name' => 'keywords',
                    'content' => 'Swap all your stuff for new in swap 254, get anything you need from a comrade!!!, find all phones on sale from University of Kabianga and sorroudning, now you can see what your next door neighbour is selling!, find food and view menus before walking in to a restourant!'
            ),
            array(
                    'name' => 'Content-type',
                    'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
            )
    );

    echo meta($meta);
    ?>
    <meta property="og:image" content="http://swap254.com/ci/style/img/logo.jpg"/>
    <meta property="og:url" content="http://swap254.com/about"/>
        
    <meta property="og:title" content="Swap254 | Swap254 is a Comrade Trade Center for University of Kabianga"/>
        
    <meta property="og:description" content="Swap all your stuff for new in swap 254, get anything you need from a comrade!!!, find all phones on sale from University of Kabianga and sorroudning, now you can see what your next door neighbour is selling!, find food and view menus before walking in to a restourant!"/>
        
</head>

<body class="container" id="page">