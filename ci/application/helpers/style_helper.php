<?php
/**
* Load css and js files
* @maxwellmandela
*/
if(!function_exists('add_css')){
    function add_css($file){
        $css = '';
        if(is_array($file)){
            foreach($file as $css){
                echo '<link rel="stylesheet" href="'.base_url().'ci/style/css/'.$css.'" type="text/css" />'."\n";
            }
        }else{
            echo '<link rel="stylesheet" href="'.base_url().'ci/style/css/'.$css.'" type="text/css" />'."\n";
        }
    }
}


if(!function_exists('add_js')){
    function add_js($file){
        $js = '';
        if(is_array($file)){
            foreach($file as $js){
                echo '<script src="'.base_url().'ci/style/js/'.$js.'"></script>'."\n";
            }
        }else{
            echo '<script src="'.base_url().'ci/style/js/'.$js.'"></script>'."\n";
        }
    }
}