<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'parse/vendor/autoload.php';

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseException;
use Parse\ParseUser;
use Parse\ParseFile;
use Parse\ParseACL;
use Parse\ParseSessionStorage;

session_start();

ParseClient::initialize('GaXvcrYd2MNHRWjxc4s98HaQ50p3M3AuKHs9TvA3', 'sheJ0cvF9CmHiAoYAPq3ibPhseMMdiR0IqteDMOw', '7LJuPbZuLUeGAYmRRkeZQo1BWYi2sqauT7iKh1Gr');

ParseClient::setStorage( new ParseSessionStorage() );

function catchError($errorCode, $errorMessage){
    switch($errorCode){
        case 6:
            print "Conection Error, please check your internet settings and refresh page";
            break;
            
        case 201:
            print "The password is missing or empty!<br>";
            print anchor("login", "Back");
            break;
            
        case 200:
            print "The username is missing or empty.<br>";
            print anchor("login", "Back");
            break;
            
        case 101:
            print "Sorry, you have used a wrong username/password combination<br>";
            print anchor("login", "Click here to try again");
            print "<p>Have you lost yourpassword? Click the link below to reset it</p>";
            print anchor("reset_password", "reset your password");
            
        default:
            print "Sorry, we've encountered an error, here's the techincal bit<br>";
            print "<p class='text text-info'>".$errorMessage."</p>";
    }
}

function userLogin(){
    if(isset($_POST["username"])){
        $username = $_POST['username'];
        $password = $_POST['password'];
    
        try {
            $nuser = new ParseUser();
            $nuser = ParseUser::logIn($username, $password);
            echo '
            <script>
            window.location.href = "http://swap254.com/";
            </script>
            ';
            
        } catch (ParseException $e) {
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);
        }
    }
}

//check login
function checkSession(){
    $user = ParseUser::getCurrentUser();
    if(empty($user)){
        ParseUser::logOut();
        echo '
        <script>
        window.location.href = "http://swap254.com/login";
        </script>
        ';
    }
}

function logOut(){
    ParseUser::logOut();
    echo '
    <script>
    window.location.href = "http://swap254.com/login";
    </script>
    ';
}

/*user info*/
function getUsername(){
    if(!empty(ParseUser::getCurrentUser())){
        return ParseUser::getCurrentUser()->username."(edit)";
    }
    return "Login";
}

function getLogOutLink(){
    if(!empty(ParseUser::getCurrentUser())){
        return "<li><a href='http://swap254.com/user/loggedout'>Log out</a></li>";
    }
    return "<li><a href='http://swap254.com/register'>Create Account</a></li>";
}

function getFirstName(){
    return ParseUser::getCurrentUser()->firstname;
}

function getLastName(){
    return ParseUser::getCurrentUser()->lastname;
}

function getResidence(){
    return ParseUser::getCurrentUser()->residence;
}

function getPhoneNumber(){
    return ParseUser::getCurrentUser()->user_phone_number;
}
function getEmail(){
    return ParseUser::getCurrentUser()->email;
}

function getSellerProfile($seller_username){
    try{
        $query = new ParseQuery("_User");
        $query->equalTo('username', $seller_username);
        $results = $query->find();
        if(count($results) > 0){
            for ($i = 0; $i < count($results); $i++) {
              $object = $results[$i];
                
              /*print $object->username;
              print $object->firstname;
              print $object->lastname;*/
              print_r($object);
            }
        }else{
            echo anchor("swap", "No user found!, click here to go back");
        }
    }catch(ParseException $e){
        $errorCode = $e->getCode();
        $errorMessage = $e->getMessage();
        catchError($errorCode, $errorMessage);
    }
}

function getItemHeaderDetails($item_id){
    //loop data
    $query = new ParseQuery("Product");
    try {
        $item = $query->get($item_id);
        $data = array(
            'title' => $item->get('product_title'),
            'price' => $item->get('product_price'),
            'description' => $item->get('product_description'),
            'seller_location' => $item->get('product_seller_location'),
            'image' => $item->get('product_image')->getUrl()
        );
        
        return $data;
        
    } catch (ParseException $ex) {
        exit("An error occured while fetching your item please bare with us");
    }
    
}

function signUp(){
    if(isset($_POST['register'])){
        try{
            $user = new ParseUser();
            $user->set("username", $_POST['username']);
            $user->set("password", $_POST['password']);
            $user->set("email", $_POST['email']);
            $user->set("residence", $_POST['residence']);

            $user->signUp();
            echo anchor("login", "Account created! Click here to login");
            
        }catch(ParseException $e){
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);
        }
    }
}

function resetPassword(){
    if(isset($_POST['reset_pwd'])){
        try{
            ParseUser::requestPasswordReset($_POST['email']);
            print "Instructions sent to your mailbox, please open the email and reset your password then head back over to login page at: <br>";
            echo anchor("login", "login");
            
        }catch(ParseException $e){
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);
        }
    }
}

function editUserProfile(){
    if(isset($_POST['edit-profile']) && $_POST['username'] != '' && $_POST['user_phone_number'] != '' && $_POST['residence'] != ''){
        try{
            $this_user = ParseUser::getCurrentUser();
            $this_user->set("username", $_POST['username']);
            $this_user->set("firstname", $_POST['firstname']);
            $this_user->set("lastname", $_POST['lastname']);
            $this_user->set("user_phone_number", $_POST['user_phone_number']);
            $this_user->set("residence", $_POST['residence']);
            
            $this_user->save();
            
            print "Profile successfully edited<br>";
            echo anchor("user/myprofile", "View new profile");
            
        }catch(ParseException $e){
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);
        }
    }else{
        echo '
        <script>
            alert("please fill in all fields to save new profile data");
        </script>
        ';
        print "please fill in all fields to save new profile data<br>";
        echo anchor("user/editmyprofile", "Back");
    }
}

function createProduct(){
    if(isset($_POST['save_item']) && $_FILES['product_image']['tmp_name'] != ''){
    
    try{
                    
        $file = ParseFile::createFromData( file_get_contents( $_FILES['product_image']['tmp_name'] ), $_FILES['product_image']['name']  );

        $imageFileType = pathinfo($_FILES['product_image']['name'],PATHINFO_EXTENSION);

        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
            print "Image does not meet requirements<br>";
            print "<p>Please either png, jpg or gif of not more than 200kb in size!</p>";
            echo anchor("swap/swap_new_item", "Back");
        }else{

            if ($_FILES["product_image"]["size"] > 200000) {
                print "Image does not meet requirements<br>";
                print "<p>Please upload either png, jpg or gif of not more than 200kb in size!</p>";
                echo anchor("swap/swap_new_item", "Back");

            }else{
                    
                $file->save();

                $newItem = new ParseObject("Product");
                $newItem->set("product_title", $_POST['product_title']);
                $newItem->set("product_price", $_POST['product_price']);
                $newItem->set("product_image", $file);
                $newItem->set("product_seller_location", ParseUser::getCurrentUser()->residence);
                $newItem->set("product_description", $_POST['product_description']);
                $newItem->set("product_category", $_POST['product_category']);
                $newItem->set("user", ParseUser::getCurrentUser());
                try{
                    $newItem->save();
                    print "Item posted successfully!!<br>";
                }catch(ParseException $e){
                    $errorCode = $e->getCode();
                    $errorMessage = $e->getMessage();
                    catchError($errorCode, $errorMessage);
                }
                print "Proccessing finished, file successfully uploaded<br>";
                echo anchor("swap/swap_new_item", "Post another!");
            }
        }
                
        }catch(ParseException $e){
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);
        }
    }else{
        print "Please select image to upload then continue<br>";
        echo anchor("swap/swap_new_item", "Back");
        print "<p>All posts must include an image</p>";
    }
}
    
function getRecentProductsList(){
    try{
        $query = new ParseQuery("Product");
        $query->notEqualTo("trash", true);
        $query->notEqualTo("sold", true);
        $query->descending("createdAt");
        $query->includeKey('user');
        
        //check number of item fetched
        if(count($query->find()) > 25 && isset($_POST['load-more'])){
            $query->skip(25);
            $results = $query->find();
        }else{
            $results = $query->find();
        }
        ?>

        <div id="products" class="hidden-xs row list-group">
            <?php
            for ($i = 0; $i < count($results); $i++) {
              $object = $results[$i];
              $seller = $object->get('user');
                ?>
                <div class="item col-xs-36" id="product">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="<?php print $object->get('product_image')->getUrl();?>" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                <a href="swap/view_item/<?php print $object->getObjectId();?>">
                                    <?php print $object->get('product_title');?></a>

                                <span class="lead text text-info">Ksh. <?php print $object->get('product_price');?></span>
                                <span class="item-date text text-right">
                                    <?php print $object->get('createdAt');?></span></h4>
                            <p class="group inner list-group-item-text">
                                <?php print $object->get('product_description');?></p>
                            <div class="group row">
                                <div class="col-sm-20 col-xs-20" style="display : table;">
                                    <span>Category: <a href="#">
                                        <?php print $object->get('product_category');?></a></span>
                                        <p class="text text-right" style="display : table-row; vertical-align : bottom;">

                                            <span class="text text-right">
                                                <span class="mdi-action-account-circle text text-primary"></span>  
                                                <a href="user/profiles/<?php print $seller->get('username');?>">
                                                    <?php print $seller->get('username');?></a></span>

                                            <a href="">
                                                <span class="mdi-communication-call text text-success"></span>  
                                                <?php print $seller->get('user_phone_number');?></a>

                                            <span class="text text-right">
                                                <span class="mdi-maps-place text text-danger"></span>  
                                                <?php print $object->get('product_seller_location');?></span>
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      

                <script>
                    $('#products .item').addClass('list-group-item');
                    $(document).ready(function() {
                        $('#products .item').addClass('list-group-item');
                        $('#list').click(function(event){
                            event.preventDefault();$('#products .item').addClass('list-group-item');});

                        $('#grid').click(function(event){
                            event.preventDefault();
                            $('#products .item').removeClass('list-group-item');
                            $('#products .item').addClass('grid-group-item');});
                    });
                </script>

            <?php
            }
        ?>
    </div>

        <div id="products-xs" class="visible-xs">
        <?php
        for ($i = 0; $i < count($results); $i++) {
          $object = $results[$i];
          $seller = $object->get('user');
            ?>
            <div class="row grid-group">
                <div class="item col-xs-36">
                    <div class="thumbnail">
                        <img class="group grid-group-image" src="<?php print $object->get('product_image')->getUrl();?>" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                <a href="swap/view_item/<?php print $object->getObjectId();?>">
                                <?php print $object->get('product_title');?></a>
                                
                                <span class="item-date"><?php print $object->get('createdAt');?></span></h4>
                            <p class="group inner list-group-item-text">
                                <?php print $object->get('product_description');?></p>
                            <div class="">
                                <p class="lead text text-info">Ksh. <?php print $object->get('product_price');?></p>
                                <p>Category: <a href="#"><?php print $object->get('product_category');?></a>,
                                    
                                    <span class="text text-right">
                                        <span class="mdi-action-account-circle text text-primary"></span>  
                                        <?php print $seller->get('username');?></span>
                                </p>
                                <p> <span class="text text-right">
                                        <span class="mdi-communication-call text text-success"></span>  
                                        <?php print $seller->get('user_phone_number');?></span>
                                    
                                    <span class="text text-right">
                                            <span class="mdi-maps-place text text-danger"></span>  
                                            <?php print $object->get('product_seller_location');?></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <script>
            $('#products .item').addClass('list-group-item');
            $(document).ready(function() {
                $('#products .item').addClass('list-group-item');
                $('#list').click(function(event){
                    event.preventDefault();$('#products .item').addClass('list-group-item');});

                $('#grid').click(function(event){
                    event.preventDefault();
                    $('#products .item').removeClass('list-group-item');
                    $('#products .item').addClass('grid-group-item');});
            });
        </script>
            
        <?php
        }
    ?></div>
    
    <?php
    }catch(ParseException $e){
        $errorCode = $e->getCode();
        $errorMessage = $e->getMessage();
        catchError($errorCode, $errorMessage); 
    }
}

function getUserProductList(){
    //list of items added by user
    try{
        $query = new ParseQuery("Product");
        $query->notEqualTo("trash", true);
        $query->notEqualTo("sold", true);
        $query->equalTo('user', ParseUser::getCurrentUser());
        $results = $query->find();
        if(count($results) > 0){
            for ($i = 0; $i < count($results); $i++) {
              $object = $results[$i];
                ?>
                <div class="thumbnail">
                    <img class="group list-group-image" src="<?php print loopItemDetail($object->get('product_image')->getUrl());?>" alt="" />
                    <div class="caption">
                        <h4 class="group inner list-group-item-heading"><?php print loopItemDetail($object->get('product_title'));?>
                            <span class="item-date"><?php print loopItemDetail($object->get('createdAt'));?></span></h4>
                        <p class="group inner list-group-item-text"><?php print loopItemDetail($object->get('product_description'));?></p>
                        <div class="row">
                            <div class="col-xs-20 col-sm-20">
                                <p class="lead text text-info">Ksh. <?php print loopItemDetail($object->get('product_price'));?></p>
                                <p>Category: <a href="#"><?php print loopItemDetail($object->get('product_category'));?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }else{
            print "You haven't added any products yet.<br>";
            echo  anchor("swap/swap_new_item", "click here to start swapping");
        }
    }catch(ParseException $e){
        $errorCode = $e->getCode();
        $errorMessage = $e->getMessage();
        catchError($errorCode, $errorMessage);    
    }
}

function searchProducts(){
    //do search
}

function deleteProductFromMyProfile(){
    //delete product added by user
}

function deleteUserAccount(){
    //delete user account
}

function getProductsFromCategory($category){
    try{
        $query = new ParseQuery("Product");
        $query->equalTo("product_category", $category);
        $query->notEqualTo("trash", true);
        $query->notEqualTo("sold", true);
        $query->descending("createdAt");
        $query->includeKey('user');
        
        //check number of item fetched
        if(count($query->find()) > 25 && isset($_POST['load-more'])){
            $query->skip(25);
            $results = $query->find();
        }else{
            $results = $query->find();
        }
        ?>

        <div id="products" class="hidden-xs row list-group">
            <?php
            for ($i = 0; $i < count($results); $i++) {
              $object = $results[$i];
              $seller = $object->get('user');
                ?>
                <div class="item col-xs-36" id="product">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="<?php print $object->get('product_image')->getUrl();?>" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                <a href="swap/view_item/<?php print $object->getObjectId();?>">
                                    <?php print $object->get('product_title');?></a>

                                <span class="lead text text-info">Ksh. <?php print $object->get('product_price');?></span>
                                <span class="item-date text text-right">
                                    <?php print $object->get('createdAt');?></span></h4>
                            <p class="group inner list-group-item-text">
                                <?php print $object->get('product_description');?></p>
                            <div class="group row">
                                <div class="col-sm-20 col-xs-20" style="display : table;">
                                    <span>Category: <a href="#">
                                        <?php print $object->get('product_category');?></a></span>
                                        <p class="text text-right" style="display : table-row; vertical-align : bottom;">

                                            <span class="text text-right">
                                                <span class="mdi-action-account-circle text text-primary"></span>  
                                                <a href="user/profiles/<?php print $seller->get('username');?>">
                                                    <?php print $seller->get('username');?></a></span>

                                            <a href="">
                                                <span class="mdi-communication-call text text-success"></span>  
                                                <?php print $seller->get('user_phone_number');?></a>

                                            <span class="text text-right">
                                                <span class="mdi-maps-place text text-danger"></span>  
                                                <?php print $object->get('product_seller_location');?></span>
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      

                <script>
                    $('#products .item').addClass('list-group-item');
                    $(document).ready(function() {
                        $('#products .item').addClass('list-group-item');
                        $('#list').click(function(event){
                            event.preventDefault();$('#products .item').addClass('list-group-item');});

                        $('#grid').click(function(event){
                            event.preventDefault();
                            $('#products .item').removeClass('list-group-item');
                            $('#products .item').addClass('grid-group-item');});
                    });
                </script>

            <?php
            }
        ?>
    </div>

        <div id="products-xs" class="visible-xs">
        <?php
        for ($i = 0; $i < count($results); $i++) {
          $object = $results[$i];
          $seller = $object->get('user');
            ?>
            <div class="row grid-group">
                <div class="item col-xs-36">
                    <div class="thumbnail">
                        <img class="group grid-group-image" src="<?php print $object->get('product_image')->getUrl();?>" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                <a href="swap/view_item/<?php print $object->getObjectId();?>">
                                <?php print $object->get('product_title');?></a>
                                
                                <span class="item-date"><?php print $object->get('createdAt');?></span></h4>
                            <p class="group inner list-group-item-text">
                                <?php print $object->get('product_description');?></p>
                            <div class="">
                                <p class="lead text text-info">Ksh. <?php print $object->get('product_price');?></p>
                                <p>Category: <a href="#"><?php print $object->get('product_category');?></a>,
                                    
                                    <span class="text text-right">
                                        <span class="mdi-action-account-circle text text-primary"></span>  
                                        <?php print $seller->get('username');?></span>
                                </p>
                                <p> <span class="text text-right">
                                        <span class="mdi-communication-call text text-success"></span>  
                                        <?php print $seller->get('user_phone_number');?></span>
                                    
                                    <span class="text text-right">
                                            <span class="mdi-maps-place text text-danger"></span>  
                                            <?php print $object->get('product_seller_location');?></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <script>
            $('#products .item').addClass('list-group-item');
            $(document).ready(function() {
                $('#products .item').addClass('list-group-item');
                $('#list').click(function(event){
                    event.preventDefault();$('#products .item').addClass('list-group-item');});

                $('#grid').click(function(event){
                    event.preventDefault();
                    $('#products .item').removeClass('list-group-item');
                    $('#products .item').addClass('grid-group-item');});
            });
        </script>
            
        <?php
        }
    ?></div>
    
    <?php
    }catch(ParseException $e){
        $errorCode = $e->getCode();
        $errorMessage = $e->getMessage();
        catchError($errorCode, $errorMessage); 
    }
}

function get_lines(){
    try{
        $query = new ParseQuery("_User");
        $query->equalTo('email', true);
        $results = $query->find();
        if(count($results) > 0){
            for ($i = 0; $i < count($results); $i++) {
              $object = $results[$i];
                ?>
                    <span class="text text"><?php print $object->get('email');?>,<br><span>
                <?php
                }
            }
        }catch(ParseException $e){
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            catchError($errorCode, $errorMessage);    
    }
}