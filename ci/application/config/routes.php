<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'swap';
$route['404_override'] = 'errors';
$route['translate_uri_dashes'] = FALSE;
