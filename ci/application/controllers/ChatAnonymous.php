<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatAnonymous extends CI_Controller {
    
    var $bots = array();
    
    public function __construct(){
        parent::__construct();
        $header_data = array(
            'title' => 'ChatAnonymous',
            'sub_title' => 'chat without a username, no one cares about  your name anymore'
        );
        
        $this->parser->parse("header-login-reg", $header_data);
    }
    
    public function index(){
        $this->load->view("chat/chat");
    }
    
    public function chat(){
        print "start new anonymous chat";
    }
}