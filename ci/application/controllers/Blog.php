<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
    function __construct(){
        parent::__construct();
        $header_data = array(
            'title' => 'Swap 254',
            'sub_title' => 'Blog - Follow Development Status, New Features'
        );
        $this->parser->parse('header-login-reg', $header_data);
        
        $this->load->view('desktop/footer');
        $this->load->view('site-footer');
    }
    
	public function index(){
        $this->load->view('blog');
	}
    
    /**
    * post in details
    */
    
    public function read_post(){
        $post_name = $this->uri->segment(3, 0);
        if(empty($post_name)){
            $error_data = array(
                'error_title' => 'Eror!',
                'message' => 'Please specify valid post title',
                'back_link' => anchor('blog', "Back to Blog")
            );
            $this->load->view('errors/post-not-found', $error_data);
        }else{
            $data = file_get_contents('http://localhost/wordpress/wp-json/posts?filter[name]='.$post_name);
            $data = json_decode($data);
            if(sizeof($data) < 1){
                $error_data = array(
                        'error_title' => 'Eror!',
                        'message' => 'No post with title: '. $post_name. '!'
                    );
                $this->load->view('errors/post-not-found', $error_data);
            }else{
                foreach($data as $post){
                $data = array('title' => $post->title,
                              'image' => $post->featured_image->guid,
                              'content' => $post->content);
                }

                $this->load->library('parser');
                $this->parser->parse('read_post', $data);   
            }
        }
    }
}
?>