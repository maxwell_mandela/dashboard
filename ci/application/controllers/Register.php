<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    
    var $header_data;
    
    function __construct(){
        parent::__construct();
        
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Create Account'
        );
    }
    
    public function loaddesktopfooterheader(){
        $this->parser->parse('header-login-reg', $this->header_data);
        $this->load->view('desktop/footer');
    }
    
    public function loadmobilefooterheader(){
        $this->parser->parse('header-login-reg', $this->header_data);
        $this->load->view('mobile/footer');
    }
    
	public function index(){
        if($this->agent->is_browser('Opera')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/register');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/sign-up');
        }
	}
    
    public function finishing(){
        $this->loadmobilefooterheader();
        signUp();
    }
}