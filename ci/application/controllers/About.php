<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
    function __construct(){
        parent::__construct();
        $header_data = array(
            'title' => 'Swap 254',
            'sub_title' => 'Sell and Buy anything within University of Kabianga, trade freely across diaspora from Kabianga to Chepnyogaa, find food, drinks and joints, new hostels'
        );
        $this->parser->parse('header-login-reg', $header_data);
    }
    
	public function index(){
        $this->load->view('landing');
        $this->load->view('desktop/footer');
        $this->load->view('site-footer');
	}
    
    public function contact_us(){
        $this->load->view('mobile/footer');
        $this->load->view('contact-us');
    }
    
    public function terms_conditions(){
        $this->load->view('mobile/footer');
        $this->load->view('terms-and-conditions');
        $this->load->view('site-footer');
    }
    
    
    public function privacy_policy(){
        $this->load->view('mobile/footer');
        $this->load->view('privacy-policy');
        $this->load->view('site-footer');
    }
    
    public function configs(){
        $this->load->view('configs');
    }
    
}
?>