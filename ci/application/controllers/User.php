<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    var $header_data;
    
    function __construct(){
        parent::__construct();
        
        $this->header_data = array(
            'title' => 'Swap 254 | My Profile'
        );
    }
    
    public function loaddesktopfooterheader(){
        $this->parser->parse('desktop/user-profile-header', $this->header_data);
        $this->load->view('desktop/footer');
    }
    
    public function loadmobilefooterheader(){
        $this->parser->parse('mobile/header', $this->header_data);
        $this->load->view('mobile/footer');
    }
    
    public function index(){
        if($this->agent->is_browser('Opera')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/view-my-profile');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/user');
        }
    }
    
    public function myprofile(){
        if($this->agent->is_browser('Opera')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/view-my-profile');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/user');
        }
    }
    
    public function editMyProfile(){
        $this->loadmobilefooterheader();
        $this->load->view('mobile/edit-my-profile');
        $this->load->view('site-footer');
    }
    
    public function editting_profile(){
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Editting Profile'
        );
        
        $this->parser->parse('header-login-reg', $this->header_data);
        editUserProfile();
        $this->load->view('site-footer');
    }
    
    function profiles(){
        $this->header_data = array(
            'title' => 'Swap 254 | Profiles'
        );

        $data = array(
            'username' => $this->uri->segment(3)
        );

        if($data['username'] == ''){
            exit('No username provided! exiting..');
        }
        
        if($this->agent->is_browser('Opera')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/view-profiles', $data);
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/view-profiles', $data);
        }
        
    }
    
    public function loggedOut(){
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Logged out'
        );
        
        $this->parser->parse('header-login-reg', $this->header_data);
        logOut();
    }
    
    public function reset_password(){
        $this->loadmobilefooterheader();
        $this->load->view('mobile/reset-password');
        $this->load->view('site-footer');
    }
    
    public function password_reset_email_sent(){
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Resettig Password'
        );
        
        $this->parser->parse('header-login-reg', $this->header_data);
        resetPassword();
        $this->load->view('site-footer');
    }
    
    public function new_profile(){
        $this->load->view('desktop/new-profile');
    }
}