<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Swap extends CI_Controller {
    
    var $header_data;
    var $detect;
    public function __construct(){
        parent::__construct();
        $this->header_data = array(
            'title' => 'Swap254 | Sell and Buy anything within University of Kabianga, trade freely across diaspora from Kabianga to Chepnyogaa, find food, drinks and joints, new hostels',
            'sub_title' => 'Swap anything with anyone at the Comrade Trade Center'
        );
        
        $this -> load -> library('Mobile_Detect');
        $this->detect = new Mobile_Detect();
    }
    
    public function loaddesktopfooterheader(){
        $this->parser->parse('header', $this->header_data);
        $this->load->view('desktop/footer');
    }
    
    public function loadmobilefooterheader(){
        $this->parser->parse('mobile/header', $this->header_data);
        $this->load->view('mobile/footer');
    }
    
    public function index(){
        if($this->agent->is_browser('Opera') || $this->detect->is('UC Browser')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/products-list');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/add-new');
            $this->load->view('desktop/buy-sell');
        }
        $this->load->view('site-footer');
    }
    
    public function categories(){
        $category_name = $this->uri->segment(3, 0);
        $data = array(
            'category_name' => $this->uri->segment(3, 0),
            'category_desc' => '',
        );
        
        if($this->agent->is_browser('Opera') || $this->detect->is('UC Browser')){
            $this->loadmobilefooterheader();
            if(empty($category_name)){
                $this->load->view('categories');
            }else{
                $this->load->view('mobile/categories', $data);
            }
        }else{
            $this->loaddesktopfooterheader();
            if(empty($category_name)){
                $this->load->view('categories');
            }else{
                $this->load->view('desktop/categories', $data);
            }
        }
        $this->load->view('site-footer');
    }
    
    public function view_item(){
        $data = array(
            'objectId' => $this->uri->segment(3)
        );

        if($data['objectId'] == ''){
            echo '<script>window.location.href = "http://swap254.com";</script>';
        }
        
        if($this->agent->is_browser('Opera') && $this->agent->is_mobile() || $this->detect->is('UC Browser')){
            $this->load->view('mobile/header');
            $this->load->view('desktop/single-item-footer');
            $this->load->view('desktop/view-item', $data);
        }else{
            $this->h_data = getItemHeaderDetails($data['objectId']);
            
            $this->load->view('desktop/view-item-header', $this->h_data);
            $this->load->view('desktop/footer');
            $this->load->view('desktop/view-item', $data);   
        }
    }
    
    public function swap_new_item(){
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Swap New Item'
        );
        
        if($this->agent->is_browser('Opera') || $this->detect->is('UC Browser')){
            $this->parser->parse('mobile/header', $this->header_data);
            $this->load->view('mobile/footer');
            $this->load->view('mobile/add-new');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/new-post');
        }
    }
    
    public function edit_item(){
        $this->header_data = array(
            'title' => 'Swap254 | Edit Item',
            'sub_title' => 'Edit Item'
        );
        
        $data = array(
            'objectId' => $this->uri->segment(4),
            'user' => $this->uri->segment(3)
        );

        if($data['objectId'] == ''){
            //echo '<script>window.location.href = "http://swap254.com";</script>';
        }
        if($this->agent->is_browser('Opera') || $this->detect->is('UC Browser')){
            $this->load->view('errors/browser-not-supported');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/edit-item', $data);
        }
    }
    
    public function add_business(){
        $this->header_data = array(
            'title' => 'Swap254 Business Directory',
            'sub_title' => 'Add Business'
        );
        
        if($this->agent->is_browser('Opera') || $this->detect->is('UC Browser')){
            $this->loadmobilefooterheader();
            $this->load->view('errors/browser-not-supported');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/add-business');
        }
    }
    
    public function finish_posting(){
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Almost finished'
        );
        
        $this->parser->parse('header-login-reg', $this->header_data);
        createProduct();
    }
    
    public function test_data(){
        /*$data = */get_lines();
        //$this->load->view('configs'/*, $data*/);
    }
    
    public function referral_program(){  //Referral
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Referral Program'
        );
        
        $this->parser->parse("header-login-reg", $this->header_data);
        $this->load->view("referral-program");
    }
}