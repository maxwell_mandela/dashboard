<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        
        $this->header_data = array(
            'title' => 'Swap254',
            'sub_title' => 'Log In'
        );
    }
    
    public function loaddesktopfooterheader(){
        $this->parser->parse('header-login-reg', $this->header_data);
        $this->load->view('desktop/footer');
    }
    
    public function loadmobilefooterheader(){
        $this->parser->parse('header-login-reg', $this->header_data);
        $this->load->view('mobile/footer');
    }
    
	public function index(){
        if($this->agent->is_browser('Opera')){
            $this->loadmobilefooterheader();
            $this->load->view('mobile/login');
        }else{
            $this->loaddesktopfooterheader();
            $this->load->view('desktop/login');
        }
	}
    
    public function finishing(){
        $this->loadmobilefooterheader();
        userLogin();
    }
}