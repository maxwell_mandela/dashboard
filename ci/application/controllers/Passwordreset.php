<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passwordreset extends CI_Controller {
    
    var $header_data;
    
    function __construct(){
        parent::__construct();
        
        $this->header_data = array(
            'title' => 'Swap 254',
            'sub_title' => 'Reset password'
        );
    }
    
    public function index(){
        $this->parser->parse('header-login-reg', $this->header_data);
        $this->load->view('mobile/reset-password');
    }
    
    public function password_reset_email_sent(){
        $this->parser->parse('header-login-reg', $this->header_data);
        resetPassword();
    }
}