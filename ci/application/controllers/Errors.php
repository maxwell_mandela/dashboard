<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {
    
    var $header_data;
    var $page_data;
    
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->header_data = array(
            'title' => '404',
            'sub_title' => 'Page not found'
        );
        $this->parser->parse("header-login-reg", $this->header_data);
        
        
        $this->page_data = array(
            'title' => '404!',
            'message' => 'Sorry, this page(<span class="text text-primary">'.current_url()/*$this->uri->segment(2, 0)*/.'</span>) has not been found!',
            'back_page' => anchor('', "Back")
        );
        $this->load->view("errors/404", $this->page_data);
    }
    
    public function no_posts(){
        $this->load->view("errors/no-posts-found", $this->page_data);
    }
    
}